<?php 
return array(
"option" => array(
	'en' => 'Enska',
	'is' => 'íslenska'
	),
"Name" => "Name",
"Email" => "Email",
"UnitAmount" => "Einingarverð",
"Qty" => "Fjöldi",
"SubTotal" => "Samtals",
"Locations" => "Staðir",
"WhyNatureFee" => "Af hverju nátturuverndargjald",
"WhyNatureFeeHomeText" => "Náttúruperlur í landi jarðarinnar Reykjahlíðar í Mývatnssveit eru komnar að þolmörkum hvað ágang  varðar. Verði ekkert að gert er fyrirséð að þessum svæðum verði lokað fyrir umferð. Slíkt er ekki til hagsbóta fyrir neinn allra síst fyrir ferðaþjónustuna og því hefur verið ákveðið að hefja gjaldtöku inn á þessi svæði til verndar og uppbyggingar.  Náttúran á þessum svæðum  við Hverina, Leirhnjúk-Kröflu og við Dettifoss ræður ekki ein og sér við  margföldun ferðamanna.  Því er nauðsyn að þessari þróun verði snúið við með þáttöku þeirra sem vilja njóta.",
"Contact" => "Hafa samband",
"MoreInformation" => "Frekari upplýsingar",
"Buy" => "Kaupa",
"Now" => "passa",
"ChooseArea" => "Veldu svæði",
"NoNatureFeeUnder18" => '<span class="green">Ekkert</span> nátturugjald <span class="green">fyrir 18 ára og yngri</span>',
"BuyNow" => "Kaupa núna",
"NoNATUREFEEAtDettifoss2014" => '<span class="bold caption-text">Ekkert náttúragjald</span> <br /><span class="">að Dettifossi</span> <br  /><span class="bold caption-text">2014</span>',
"DettifossLocationSmallText"=>"Stórkostlegt umhverfi Jökulsárgljúfra er mótað af vatni, eldum og ís. Gífurleg hamfarahlaup eru talin hafa myndað og mótað gljúfur, gil, klappir og byrgi. Frægast þeirra er Ásbyrgi. Vatnajökulsþjóðgarður er ... ",
"HverirLocationSmallText" => "Námafjall og umhverfi er háhitasvæði og er jarðhitasvæðið er eitt fjölsóttasta hverasvæði á Íslandi. Svæðið liggur um sprungugrein sem nær norðan úr Öxarfirði gegnum eldstöðina Kröflu og suður fyrir ...",
"LeirhnjukurLocationSmallText" => "Kröflueldar voru hrina sprungugosa og kvikuhlaupa, sem gengu yfir með hléum frá 20. desember 1975 til 18. september 1984. Gossprungur náðu frá Leirhnjúki í miðri  Kröfluöskjunni norður í Gjástykki. Lengd þeirra er ...",
"Action" => 'Aðgerðir',
"Update" => 'Uppfæra',
"Delete" => 'Eyða',
"Information" => 'Upplýsingar',
'Register' => 'Skrá',
'Pay' => 'Greiða',
"Creditcard" => "Kreditkort",
"FreeOnDettifoss" =>"Frítt inn að Dettifossi 2014",
"Name" => "Nafn",
"Qty" => "Fjöldi",
"Products" => "Vörur",
"Buyer" => "Kaupandi",
"Email" => "Netfang",
"Order" => "Pöntun",
"Receipt" => 'Kvittun',
'Photos' => 'Myndir: <a href="http://www.volcanic-springs.com/">Dirk Nierman</a> og fleirri',
'ContactUs' => 'Hafðu samband',
'Message' => 'Skilaboð',
'SendMessage' => 'Senda skilaboð',
'WhyNatureFeeParagraphs'=> '<p class="whyText">Undanfarin ár hefur fjöldi ferðamanna inn á vinsælustu náttúruperlur Reykjahíðar í Mývatnssveit margfaldast.  Nú eru svæðin við, Hverina, Leirhnjúk-Kröflu og Dettifoss, komin að þolmörkum og engan vegin sjálfbær.</p>
							<p class="whyText">Því er nauðsynlegt að hefja uppbyggingu á þessum svæðum ásamt því að veita þjónustu fyrir ferðamenn á viðkomandi svæðum. Um leið er náttúran sjálf höfð í fyrirrúmi.</p>
							<p class="whyText">Náttúruverndargjaldinu verður varið til uppbyggingar á þjónustuhúsum með salernum, veitingaaðstöðu ofl. Að auki verða lagði göngustígar, útsýnispallar og útskot þar sem við á. Þetta er jafnframt liður í því að bæta öryggi ferðamanna eins og kostur er. </p>
							<p class="whyText">Markmið er að allir þessi staðir veiti betri þjónustu fyrir gesti en er í dag, ásamt því að vernda  náttúruna til framtíðar.</p>
							<p class="whyText">Það er von okkar að þú takir þátt í því að byggja upp þessar fallegu náttúruperlur okkar með framlagi þínu.</p>
							<p class="whyText bold">Taktu þátt í því að vernda náttúru Íslands.</p>
							<p class="whyText bold">Verið velkomin og njótið náttúru Reykjahlíðar í Mývatnssveit.</p>',
'SeeMorePictures' => 'Sjá fleiri myndir',
'Confirmation' => 'Pöntunin',
'ThankYouForTheOrder'=>'Takk fyrir að panta!',
'YourCodeIs' => 'Hér með er staðfest að pöntunin hefur farið í gegn.',
'Number' => 'Númerið er <span class="bold">:number</span>',
'Footerline' => 'Eigandi: Landeigendur Reykjahlíðar ehf. ',
'Valid' => 'Gildistími: :valid',
'Ticket' => 'Miði',
'PleaseBringThisTicketYou'=>'Vinsamlegast takið miðann með.',
'Label' => 'Númer',
'QRCode' => 'QR kóðir',




);

?>