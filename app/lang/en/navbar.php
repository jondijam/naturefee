<?php 
return array(
"option" => array(
	'en' => 'English',
	'is' => 'íslenska'
	),
"Name" => "Name",
"Email" => "Email",
"UnitAmount" => "Unit prize",
"Qty" => "Quantitum",
"SubTotal" => "Total",
"Locations" => "Locations",
"WhyNatureFee" => "Why nature fee",
"WhyNatureFeeHomeText" => "The natural treasures of Reykjahlíð in Mývatn have reached the point of of no return in terms of visitor numbers. If nothing is done then its likely that these areas will have to be closed to tourists. This situation wouldn't benefit anyone, least of all the tourist industry, and therefore the decision has been taken to begin charging a fee in these areas, to help protect and develop them. The natural environments of Hverina, Leirhnjúk-Kröflu and Dettifoss can no longer handle the growth tourist numbers alone. It is therefore necessary to tackle this situation with the help of those who come to enjoy these areas.",
"Contact" => "Hafa samband",
"MoreInformation" => "More information",
"Buy" => "Buy",
"Now" => "now",
"ChooseArea" => "Choose area",
"NoNatureFeeUnder18" => '<span class="green">No</span> NATURE<span class="green"> FEE UNDER 18 YEARS OLD</span>',
"BuyNow" => "Buy now",
"NoNATUREFEEAtDettifoss2014" => '<span class="bold caption-text">No NATUREFEE</span> <br /><span class="">at Dettifoss</span> <br  /><span class="bold caption-text">2014</span>',
"DettifossLocationSmallText"=>"The magnificent environment of Jökulsá canyon is shaped by water, fire and ice. Huge, catastrophic floods are believed to have formed and shaped the canyon, and its gullies, low flat rocks and shelters...",
"HverirLocationSmallText" => "The Námafjall mountain area is a high temperature region and one of the most visited geothermal spots in Iceland. It lies along a fissure section which extends north from Öxarfjörður through the Krafla volcano...",
"LeirhnjukurLocationSmallText" => "The Krafla Fires were a series of fissure eruptions and magma flows which occurred during the period of 20 December 1975 to 18 September 1984. The fissures reach from Leirhnjúki in the middle of the Krafla...",
"Action" => 'Action',
"Update" => 'Update',
"Delete" => 'Delete',
"Information" => 'Information',
'Register' => 'Register',
'Pay' => 'Pay',
"Creditcard" => "Creditcard",
"FreeOnDettifoss" =>"No fee at Dettifoss 2014",
"Products" => "Products",
"Buyer" => "Buyer",
"Email" => "Email",
"Order" => "Order",
"Receipt" => 'Receipt',
'Photos' => 'Photos: <a href="http://www.volcanic-springs.com/">Dirk Nierman</a> and others',
'ContactUs' => 'Contact us',
'Message' => 'Message',
'SendMessage' => 'Send message',
'WhyNatureFeeParagraphs' => '<p class="whyText">In recent years the number of tourists to the popular natural wonders of Reykjahlíð in Mývatn has multiplied. Now, the areas around Hverina, Leirhnjúk-Kröflu and Dettifoss have reached a threshold that is no longer sustainable.It is therefore necessary to start developing these areas and to provide services for tourists where needed. In this way nature itself can remain the focus.</p>
							<p class="whyText">The conservation fee will be used to build service centres, with toilets and catering facilities etc. In addition walking trails will be laid, and terraces and rest areas provided where appropriate. This is also part of improving tourist safety as much as possible.</p>
							<p class="whyText">The goal is for all of these sites to provide a better service for today\'s guests, as well as protecting the natural environment for the future.</p>
							<p class="whyText">It is our hope that you will play your part in developing these beautiful natural wonders, through your contribution.</p>	
							<p class="whyText bold">Take part in protecting Icelandic nature.</p>
							<p class="whyText bold">Be welcome and enjoy the natural environment of Reykjahlíð in Mývatn.</p>',
'SeeMorePictures' => 'See more pictures',
'Confirmation' => 'Confirmation',
'ThankYouForTheOrder'=>'Thank you for order',
'YourCodeIs' => 'You will get your number in email that you need to bring to office',
'Number' => 'The number is <span class="bold">:number</span>',
'Footerline' => 'Owner: Landeigendur Reykjahlíðar ehf.',
'Valid' => 'Valid until: :valid',
'Ticket' => 'Ticket',
'PleaseBringThisTicketYou'=>'Please bring this ticket with you.',		
'Label' => 'Label',
'QRCode' => 'QR code',
);

?>