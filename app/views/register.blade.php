<!DOCTYPE html>
<html lang="en">
  <head>
    @include('head')
  </head>
  <body>
  	 <div class="navbar navbar-default">
  	 	<div class="container">
  	 		<div class="navbar-header">
	          <a href="/" class="navbar-brand">looll.is</a>
	          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	        </div>
	        <div class="navbar-collapse collapse" id="navbar-main">
	          <ul class="nav navbar-nav navbar-right">
	            <li class="active">
	              <a href="/register">@lang("Register")</a>
	            </li>
	            <li>
	              <a href="/login">@lang("Login")</a>
	            </li>
	          </ul>
	        </div>
  	 	</div>
  	 </div>
  	 <div class="container">
  	 	<div class="row">
  	 		<div class="col-sm-3">
  	 			
  	 		</div>
  	 		<div class="col-sm-5">
          @if($errors->all())
            <div class="alert alert-danger">
            <p>
            {{{
              $errors->first('username')
            }}}
            </p>
            <p>
            {{{
              $errors->first('password')
            }}}
            <p>
            {{{
              $errors->first('email')
            }}}
            </p>
            </p>
            </div>
          @endif
 
  	 			<form action="/register/save" method="post">
  	 				<div class="form-group">
  	 					<label>@lang('looll.Username')</label>
  	 					<input type="text" class="form-control" name="username" value="" />
  	 				</div>
  	 				<!--<div class="form-group">
  	 					<label>@lang('looll.Roles')</label>
  	 					<select name="roles" class="form-control">
  	 						<option value="1">@lang('looll.Organisation')</option>
  	 						<option value="2">@lang('looll.Individual')</option>
  	 					</select>
  	 				</div>
  	 				-->
  	 				<div class="form-group">
  	 					<label>Email</label>
	  	 				<input type="text" name="email" class="form-control" method="post" />
  	 				</div>
  	 				<div class="form-group">
  	 					<label>@lang('looll.Password')</label>
  	 					<input type="password" name="password" class="form-control" />
  	 				</div>
  	 				<div class="form-group">
  	 					<label>@lang('looll.ConfirmPassword')</label>
  	 					<input type="password" name="password_confirmation" class="form-control" />
  	 				</div>
  	 				<div class="form-group">
  	 					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  	 					<input type="submit" class="btn btn-primary" value="@lang('looll.Register')" />
  	 				</div>
  	 			</form>
  	 		</div>
  	 	</div>
  	 </div>
  @include('footer')
  </body>
 </html>
