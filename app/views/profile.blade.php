<!DOCTYPE html>
<html lang="en">
  <head>
    @include('head')
  </head>
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a href="/" class="navbar-brand">looll.is</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
           <ul class="nav navbar-nav navbar-right">
              @if($loggedin)
            <ul class="nav navbar-nav navbar-right">
              @if(count($count_user_organisations) > 0)
              <li>
                <a href="/organisation">@lang('looll.Organisation')</a>
              </li>
              @endif
              <li>
                <a href="/profile">@lang('looll.MyProfile')</a>
              </li>
               <li>
                <a href="/account">@lang('looll.Account')</a>
              </li>
              <li>
                <a href="/logout">@lang('looll.Logout')</a>
              </li>

            </ul>
          @else
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a href="/register">@lang('looll.Registration')</a>
            </li>
            <li>
              <a href="/login">@lang('looll.Login')</a>
            </li>
          </ul>
          @endif
          </ul>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h1>{{{$name}}}</h1> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          @if(count($images) > 0)
            @foreach($images as $image)
              <div class="thumbnail">
                <img src="{{{$image->src}}}" alt="{{{$image->name}}}" class="img-thumbnail" />
              </div>
            @endforeach
          @endif
        </div>
        <div class="col-sm-2">
          <address>
            {{{$address}}}<br>
            {{{$zip}}} {{{$city}}}<br>
            {{{$country}}}        
          </address>
        </div>
        <div class="col-sm-3">
           <address>
            @if(!empty($position))
            <strong>@lang('looll.Position'):</strong>
            <span class="pull-right">{{{$position}}}</span>
            <br />
            @endif
            @if(!empty($birthday))
            <strong>@lang('looll.Birthday'):</strong>
            <span class="pull-right">{{{$birthday}}}</span>
            <br />
            @endif
            @if(!empty($email))
            <strong>@lang('looll.Email'):</strong>
            <span class="pull-right">{{{$email}}}</span>
            <br />
            @endif
            @if(!empty($telephone_number))
              <strong>@lang('looll.Tel'):</strong>
              <span class="pull-right">{{{$telephone_number}}}</span>
              <br />
            @endif
            @if(!empty($mobile_number))
              <strong>@lang('looll.Mobile'):</strong>
              <span class="pull-right">{{{$mobile_number}}}</span>
              <br />
            @endif
            @if(!empty($buisness_phone_number))
              <strong>@lang('looll.Work'):</strong>
              <span class="pull-right">{{{$buisness_phone_number}}}</span>
              <br />
            @endif
           </address>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9 col-sm-offset-3">
                  <hr />
                  @if(!empty($description))
                  <div class="well">
                  <h4 style="margin-bottom: 0.4em;">@lang('looll.Information')</h4>
                  <p>{{{$description}}}</p>
                  </div>
                  @endif
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9 col-sm-offset-3">
            @if(count($organisations))
                <div class="well">
                <h4 style="margin-bottom: 0.4em;">@lang('looll.MemberOfTheseOrganisations')</h4>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>@lang('looll.Name')</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                      @foreach($organisations as $organisation)
                        <tr>
                          <td><a href="/organisation/{{{$organisation->organisation_id}}}">{{{$organisation->name}}}</a></td>
                        </tr>
                      @endforeach
                    
                    <tr>
                    </tr>
                  </tbody>
                </table>
                </div>
                @endif
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9 col-sm-offset-3">
             @if(count($contact_organisations) > 0)
                <div class="well">
                
                <h4 style="margin-bottom: 0.4em;">@lang('looll.IAmContactOfTheseOrganisations') </h4>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>@lang('looll.Name')</th>
                    </tr>
                  </thead>
                  <tbody>
                  
                      @foreach($contact_organisations as $contact_organisation)
                      <tr>
                        <td><a href="/organisation/{{{$contact_organisation->organisation_id}}}">{{{$contact_organisation->name}}}</a></td>
                      </tr>
                      @endforeach
                    
                  </tbody>
                </table>
                </div>
                @endif
        </div>
      </div>
    </div>
  </body>
</html>