<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="Icelandic Nature - Nature Fee">
		<meta name="keywords" content="Nature, Icelandic Nature, Nature Iceland, Iceland">
		<meta name="author" content="www.bgunnarsson.com">

		<title>Leirhnjúkur - Locations - Nature Fee</title>

		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">

		<style type="text/css">
			body {
				padding-top: 65px;
			}
			.grid .test {
				background: #ddd;
			}
			a {
				color: #6a5a8c;
			}
			a:hover {
				color: #6a5a8c;
			}
		</style>
		<!-- <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="assets/css/animate.css" rel="stylesheet">
		<!-- <link href="assets/css/lightbox.css" rel="stylesheet"> -->
		<link href="assets/css/naturefee.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body class="subPage">
		<nav id="header" class="navbar navbar-fixed-top" role="navigation">
			<div class="headerWrapper">
				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<h1 class="logoH1">
							<a href="/" id="logo" class="navbar-brand logoToTop" title="Nature Fee">
								<img src="assets/img/logo-is.png" alt="Nature Fee" />
							</a><!-- .navbar-brand -->
						</h1>
						<a href="" id="mobile-nav-trigger" class="pull-right">
							<i class="fa fa-bars"></i>
						</a><!-- #mobile-nav-trigger -->
					</div><!-- .navbar-header -->

					<div id="arcade-desktop-nav" class="arcadeNav pull-right clearfix">
						<ul class="navList">
							<li>
								<a href="/" class="">
									<span data-hover="Heim">Heim</span>
								</a>
							</li>
							<li>
								<a href="/">
									<span data-hover="@lang('navbar.Locations')">@lang('navbar.Locations')</span>
								</a>
							</li>
							<li>
								<a href="/">
									<span data-hover="@lang('navbar.WhyNatureFee')?">@lang('navbar.WhyNatureFee')?</span>
								</a>
							</li>
							<li>
								<a href="/">
									<span data-hover="@lang('navbar.Contact')">@lang('navbar.Contact')</span>
								</a>
							</li>
							<li id="natureLang">
								<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
									<i class="fa fa-globe"></i> <span data-hover="{{{strtoupper($locale)}}}">{{{strtoupper($locale)}}}</span> <i class="fa fa-caret-down"></i>
								</a>
								<ul id="langList">
									@foreach(Lang::get( 'navbar.option' ) as $key => $option)
										
										@if($locale != $key)
											<li>
												<a href="@if($locale == "is")http://naturefee.com/leirhnjukur @endif" class="langItem lang-{{{$key}}}">
													<span data-hover="{{{strtoupper($key)}}}">{{{strtoupper($key)}}}</span>
												</a>
											</li>
										@endif
									@endforeach
									
								</ul>
							</li><!-- #natureLang -->
						</ul><!-- .navList -->
					</div><!-- #arcade-desktop-nav -->

					<div id="arcade-mobile-nav" class="clearfix">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="navList">
										<li>
											<a href="" id="navHome" class="navItem" data-section="01">
												<span data-hover="@lang('navbar.Locations')">@lang('navbar.Locations')</span>
											</a>
										</li>
										<li>
											<a href="" id="navAbout" class="navItem" data-section="02">
												<span data-hover="@lang('navbar.WhyNatureFee')?">@lang('navbar.WhyNatureFee')?</span>
											</a>
										</li>
										<li>
											<a href="" id="navContact" class="navItem" data-section="03">
												<span data-hover="@lang('navbar.Contact')">@lang('navbar.Contact')</span>
											</a>
										</li>
										<li id="natureLang">
										<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
											<i class="fa fa-globe"></i> <span data-hover="{{{strtoupper($locale)}}}">{{{strtoupper($locale)}}}</span> <i class="fa fa-caret-down"></i>
										</a>
										<ul id="langList">
											@foreach(Lang::get( 'navbar.option' ) as $key => $option)
												
												@if($locale != $key)
													<li>
														<a href="/@if($locale == "is"){{{$key}}}@endif" class="langItem lang-{{{$key}}}">
															<span data-hover="{{{strtoupper($key)}}}">{{{strtoupper($key)}}}</span>
														</a>
													</li>
												@endif
											@endforeach
											
										</ul>
									</li><!-- #natureLang -->
									</ul><!-- .navList -->
								</div><!-- .col-md-12 -->
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- #arcade-mobile-nav -->

				</div><!-- .container -->
			</div><!-- .headerWrapper -->
		</nav><!-- .navbar -->
		<!-- start: COVER-->
		<div id="section_04" class="cover">
		</div><!-- #cover -->
		<!-- end: COVER -->
		<!-- start: SECTION 01 -->
		<div id="section_01" class="section section-leirhnjukur">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<spna>Leirhnjúkur-Krafla</spna>
							<hr class="right visible-md visible-lg" />
						</h2>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->
				
				<!-- start: SUB PAGE  -->
				<div class="row">
					<div class="col-sm-6 pull-right">
						<img src="assets/img/leirhnjukur-big.jpg" alt="Leirhnjúkur - Nature Fee" />
					</div><!-- .col-sm-6 -->
					<div class="col-sm-6 pull-left">
							<p class="subPageText">Kröflueldar voru hrina sprungugosa og kvikuhlaupa, sem gengu yfir með hléum frá 20. desember 1975 til 18. september 1984. Gossprungur náðu frá Leirhnjúki í miðri  Kröfluöskjunni norður í Gjástykki. Lengd þeirra er um 11 km, þar af  7,5 km norðan öskjunnar. Flatarmál hraunanna er um 35 km². Á sama tíma var verið að byggja Kröfluvirkjun með tilheyrandi borunum eftir gufu til að knýja virkjunina.</p>

							<p class="subPageText">Krafla er megineldstöð með öskju, sem er að miklu leyti fyllt yngri jarðmyndunum. Í jarðskorpunni undir Leirhnjúk er kvikuhólf á um þriggja kílómetra dýpi. Þar safnast fyrir basísk kvika. Í umbrotahrinum treðst kvikan eftir sprungum í jarðskorpunni, ýmist til suðurs eða norðurs, eða þá að hún berst upp á yfirborð í eldgosum. Alls náði kvika níu sinnum til yfirborðs í Kröflueldum:</p>

							<ul class="locationTextList">
								<li>1975, 1. gos: 20. til 21. desember</li>
								<li>1977, 2. gos: 27. til 29. apríl</li>
								<li>1977, 3. gos: 8. til  9. september</li>
								<li>1980, 4. gos: 16. mars</li>
								<li>1980, 5. gos: 10. til 18. júlí</li>
								<li>1980, 6. gos: 18. til 23. október</li>
								<li>1981, 7. gos: 30. janúar til 4. febrúar</li>
								<li>1981, 8. gos: 18. til 23. nóvember</li>
								<li>1984, 9. gos: 4. til 18. september</li>
							</ul>

							<p class="subPageText">Kröflueldar voru sama eðlis og Mývatnseldar 1724-1729. Þá héldust gosin innan öskjunnar nema í einni hrinunni þegar gaus sunnan hennar. Þá kom smáhraun upp vestan við Námafjall. Frá Kröflu rann hraun niður í byggð og eyddi tveim bæjum.</p>

							<p class="subPageText">Kröflueldar hófust með litlu hraungosi við Leihnjúk. Í kjölfarið fylgdi mikil hrina jarðskjálfta með sprunguhreyfingum og landsigi í Kelduhverfi. Á næstu níu árum urðu tuttugu misstórar hrinur í eldstöðvakerfinu. Mikið magn kviku tróðst  inn í rúmlega 70 km langan kafla af spungusveimi þess og land gliðnaði um nokkra metra frá Jökulsársöndum í norðri suður fyrir Hverfell. Gliðnunin nam mest 9 metrum við norðurjaðar öskjunnar. </p>

							<p class="subPageText">Í Kröflueldum gafst einstakt tækifæri til að rannsaka landrek í eldstöðvakerfi á flekaskilum. Skiluðu Kröflueldar því mikilli þekkingu á eldvirkni og landreki.</p>

							<p class="subPageText">Öflug jarðhitasvæði eru í Kröfluöskjunni og Námafjalli. Kvikuinnskot kynda undir. Jarðhitinn hefur verið virkjaður til raforkuframleiðslu í Kröflustöð og Bjarnarflagi.</p>

							<a href="http://natturugjald.is/" class="btn btn-success btn-lg"><i class="fa fa-chevron-left"></i> Til baka</a>
					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<!-- end: SUB PAGE  -->

			</div><!-- .container -->
		</div><!-- #section_01 -->
		<!-- end: SECTION 01 -->

		<!-- start: FOOTER -->
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="footerInner">
								<span class="footerSep">&nbsp;</span>
								@lang('navbar.Footerline')
								<span class="footerSep">&nbsp;</span> @lang('navbar.Photos')
							</div><!-- .footerInner -->
						</div><!-- .col-sm-12 -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- #footer -->
			<!-- end: FOOTER -->

		<!-- start: MEDIA QUERY TESTER -->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mediaQuery">
						MediaQuery!
					</div><!-- .mediaQuery -->
				</div><!-- .col-sm-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
		<!-- end: MEDIA QUERY TESTER -->
		
		<!-- start: TO THE TOP -->
		<a href="" class="toTheTop" title="Fara efst">
			<i class="fa fa-chevron-up"></i>
		</a>
		<!-- end: TO THE TOP -->


		<!-- javascript
		================================================== -->
		<script src="/assets/js/jquery1102.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<script src="/assets/js/denta-scrolltop.js"></script>
		<script src="/assets/js/jquery-scrollto.js"></script>
		<script src="/assets/js/naturefee.js"></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-46042256-1', 'bgunnarsson.com');
			ga('send', 'pageview');
		</script>
	</body>
</html>