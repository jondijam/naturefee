<!DOCTYPE html>
	<html lang="en">
		<head>
			 @include('head')
		</head>
		<body>
			<div class="navbar navbar-default">
				<div class="container">
					<div class="navbar-header">
						<a href="/" class="navbar-brand">looll.is</a>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
							<span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-main">
						<ul class="nav navbar-nav navbar-right">
							<li class="active">
								<a href="/organisation">@lang('looll.Organisation')</a>
							</li>
							<li>
								<a href="/profile">@lang('looll.MyProfile')</a>
							</li>
							<li>
								<a href="/account">@lang('looll.Account')</a>
							</li>
							<li>
								<a href="/logout">@lang('looll.Logout')</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div id="result">
							@if(count($images))
								@foreach($images as $image)
								<div class="imageholder">
									<figure>
										<img src="{{{$image->src}}}" alt="{{{$image->name}}}" class="image" data-image-id="{{{$image->image_id}}}"/>
										<figcaption>
											{{{$image->name}}} <br/>
											
										</figcaption>
									</figure>
								</div>
								@endforeach
							@endif
						</div>
					</div>
					<div class="col-sm-5">
						<form action="" method="post">
							<fieldset>

								<h1>@lang('looll.Organisation')</h1>
								@if($organisation_id > 0)
								<div id="droparea">
									<div class="dropareainner">
										<p class="dropfiletext">@lang('looll.DropFilesHere')</p>
										<p>@lang('looll.Or')</p>
										<p>
										<input id="uploadbtn" type="button" value="@lang('looll.SelectFiles')"/></p>
										<!-- extra feature -->
										<p id="err"><!-- error message -->
										</p>
									</div>
									<input id="upload" type="file" multiple="">
								</div>
								@else
								<p>You need to save your organisation to add images.</p>
								@endif
								<div class="form-group">
									<label>@lang('looll.Name')</label>
									<input type="text" name="name" id="name" class="form-control" value="{{{$name}}}" />
								</div> 
								<div class="form-group">
									<label>@lang('looll.Description')</label>
									<textarea class="form-control" id="description" name="description">{{{$description}}}</textarea>
								</div>
								<div class="form-group">
									<label>@lang('looll.Information')</label>
									<textarea class="form-control" name="information" id="information">{{{$information}}}</textarea>
								</div>
								<div class="form-group">
									<label>@lang('looll.Address')</label>
									<input type="text" class="form-control" name="address" id="address" value="{{{$address}}}" />
								</div>
								<div class="form-group">
									<label>@lang('looll.City')</label>
									<input type="text" class="form-control" name="city" id="city" value="{{{$city}}}" />
								</div>
								<div class="form-group">
									<label>@lang('looll.Zip')</label>
									<input type="text" class="form-control" name="zip" id="zip" value="{{{$zip}}}" />
								</div>
								<div class="form-group">
									<label>@lang('looll.Country')</label>
									<input type="text" class="form-control" name="country" id="country" value="{{{$country}}}" />
								</div>
								<div class="form-group">
									<label>Type of members</label>
									<select name="membersType" id="membersType" class="form-control">
										<option @if($membersStatus == 1) selected="selected" @endif value="1">No members can join</option>
										<option @if($membersStatus == 2) selected="selected" @endifvalue="2">People can join</option>
										<option @if($membersStatus == 3) selected="selected" @endifvalue="3">Organisations can join</option>
									</select>
								</div>
								<div class="form-group">
									<label>@lang('looll.MemberOf')</label>
									<div style="clear:both">
										@if(count($parent_organisations) > 0)
											<select name="organisation_parent" id="organisation_parent" class="form-control">
												<option value="0">
													@lang('looll.NoOrganisationSelected')
												</option>
												@foreach($parent_organisations as 
												$organisation)
													<option value="{{$organisation->organisation_id}}" @if($parent_id == $organisation->organisation_id) selected="selected" @endif>
													{{{$organisation->name}}}
													</option>
												@endforeach()	
											</select>
										@endif
									</div>
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-primary" name="save" data-organisation-id="{{{$organisation_id}}}" id="save" value="@lang('looll.Save')" />
								</div>
							</fieldset>
						</form>
						@if($organisation_id > 0)
							@if($membersStatus == 3)

							<h3>Organisation members</h3>
							<table class="table">
								<thead>
									<tr>
										<th>Organisation</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@if(count($organisations_children) > 0)
										@foreach($organisations_children as $organisation)
											<tr>
											<td>
												{{{$organisation->name}}}
											</td>
											<td>
												@if($organisation->parent_active == -1)
													<button data-organisation-id="{{$organisation->organisation_id}}" id="accept{{$organisation->organisation_id}}" class="accept btn btn-primary">Accept</button>
													<button data-organisation-id="{{$organisation->organisation_id}}" id="decline{{$organisation->organisation_id}}" class="decline btn btn-warning">Declined</button>
												@elseif($organisation->parent_active == 1)
													
													<button data-organisation-id="{{$organisation->organisation_id}}" id="accept{{$organisation->organisation_id}}" class="accept hidden btn btn-primary">Accept</button>
													<button data-organisation-id="{{$organisation->organisation_id}}" id="decline{{$organisation->organisation_id}}" class="decline btn btn-warning">Declined</button>
												@elseif($organisation->parent_active == 0)
													<button data-organisation-id="{{$organisation->organisation_id}}" id="accept{{$organisation->organisation_id}}" class="accept btn btn-primary">Accept</button>
													<button data-organisation-id="{{$organisation->organisation_id}}" id="decline{{$organisation->organisation_id}}" class="decline hidden btn btn-warning">Declined</button>													
												@endif
								
											</td>
										</tr>
										@endforeach
									@endif
								</tbody>
							</table>
							@elseif($membersStatus == 2)
							<h3>User members</h3>
							<table class="table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@if(count($users) > 0)
										@foreach($users as $user)
											
											<tr id="deleteUser{{$user->user_id}}">
												<td>
													{{{$user->profile->name}}}
												</td>
												<td>
													@if($user->pivot->member == -1 )
													
														<button data-user-id="{{$user->user_id}}" data-organisation-id="{{$organisation_id}}" id="acceptUser{{$user->user_id}}" class="acceptUser btn btn-primary">Accept</button>
														<button data-user-id="{{$user->user_id}}" data-organisation-id="{{$organisation_id}}" id="declineUser{{$user->user_id}}" class="declineUser btn btn-warning">Declined</button>
														
													@elseif($user->pivot->member == 0 )
														<button data-user-id="{{$user->user_id}}" data-organisation-id="{{$organisation_id}}" id="acceptUser{{$user->user_id}}" class="acceptUser btn btn-primary">Accept</button>
														<button data-user-id="{{$user->user_id}}" data-organisation-id="{{$organisation_id}}" id="declineUser{{$user->user_id}}" class="declineUser hidden btn btn-warning">Declined</button>
												
													@elseif($user->pivot->member == 1 )
														<button data-user-id="{{$user->user_id}}" data-organisation-id="{{$organisation_id}}" id="acceptUser{{$user->user_id}}" class="acceptUser hidden btn btn-primary">Accept</button>
														<button data-user-id="{{$user->user_id}}" data-organisation-id="{{$organisation_id}}" id="declineUser{{$user->user_id}}" class="declineUser btn btn-warning">Declined</button>
						
													@endif

													<button class="deleteUser btn btn-warning" data-user-id="{{$user->user_id}}" data-organisation-id="{{$organisation_id}}">@lang('looll.Delete')</button>
													
												</td>
											</tr>
										@endforeach
									@else
									<tr>
										<td colspan="2">
											@lang('looll.NoUsersHasBeenAdded')
										</td>
									</tr>	
									@endif
								</tbody>
							</table>
							@endif
						@endif
					</div>
				</div>
			</div>
			@include('footer')
			<script type="text/javascript" src="/js/organisation.js"></script>
			<script src="/js/modernizr.custom.js"></script>
			<script src="/js/script.js"></script>
			<script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
			<script id="imageTemplate" type="text/x-jquery-tmpl"> 
				 <div class="imageholder">
					<figure>
						<img src="${filePath}" alt="${fileName}" class="image" data-image-id="${image_id}"/>
						<figcaption>
							${fileName} <br/>
							<span>Original Size: ${fileOriSize} KB</span><br/>
							<span>Upload Size: ${fileUploadSize} KB</span>
						</figcaption>
					</figure>
				</div>
			</script>
		</body>
	</html>