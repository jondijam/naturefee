<!DOCTYPE html>
<html lang="en">
  <head>
    @include('head')
  </head>
  <body>
  	<div class="navbar navbar-default">
  		<div class="container">
  	 		<div class="navbar-header">
	          <a href="/" class="navbar-brand">looll.is</a>
	          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	        </div>
	        <div class="navbar-collapse collapse" id="navbar-main">
	        <ul class="nav navbar-nav navbar-right">
	            <li>
	            	<a href="/register">@lang("Register")</a>
	            </li>
	            <li  class="active">
	            	<a href="/login">@lang("Login")</a>
	            </li>
	        </ul>
	        </div>
	    </div>
  	</div>
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-3">
  	 		</div>
  	 		<div class="col-sm-5">
  	 			<form action="/login" method="post">
  	 				<div class="form-group">
  	 					<label>@lang('looll.Username')</label>
  	 					<input type="text" class="form-control" name="username" value="" />
  	 				</div>
  	 				<div class="form-group">
  	 					<label>@lang('looll.Password')</label>
  	 					<input type="password" class="form-control" name="password" value="" />
  	 				</div>
  	 				<div class="form-group">
					    <div class="checkbox">
					        <label>
					          <input type="checkbox" name="remember" value="1"> @lang('looll.RememberMe')
					        </label>
					    </div>
					</div>
  	 				<div class="form-group">
  	 					<input type="submit" class="btn btn-primary" name="login" value="@lang('looll.Login')" />
  	 					<a href="">@lang('looll.ForgetYourPassword')</a>
  	 				</div>
  	 			</form>
  	 		</div>
  		</div>
  	</div>
  </body>
 </html>