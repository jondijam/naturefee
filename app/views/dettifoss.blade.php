<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="Icelandic Nature - Nature Fee">
		<meta name="keywords" content="Nature, Icelandic Nature, Nature Iceland, Iceland">
		<meta name="author" content="B. Gunnarsson">

		<title>Dettifoss - Locations - Nature Fee</title>

		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">

		<style type="text/css">
			body {
				padding-top: 65px;
			}
			.grid .test {
				background: #ddd;
			}
			a {
				color: #6a5a8c;
			}
			a:hover {
				color: #6a5a8c;
			}
		</style>
		<!-- <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="assets/css/animate.css" rel="stylesheet">
		<link href="assets/css/naturefee.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
		<![endif]-->

	</head>

	<body class="subPage">

		<nav id="header" class="navbar navbar-fixed-top" role="navigation">
			<div class="headerWrapper">
				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<h1 class="logoH1">
							<a href="/" id="logo" class="navbar-brand" title="Nature Fee">
								<img src="assets/img/logo-is.png" alt="Nature Fee" />
							</a><!-- .navbar-brand -->
						</h1>
						<a href="" id="mobile-nav-trigger" class="pull-right">
							<i class="fa fa-bars"></i>
						</a><!-- #mobile-nav-trigger -->
					</div><!-- .navbar-header -->

					<div id="arcade-desktop-nav" class="arcadeNav pull-right clearfix">
						<ul class="navList">
							<li>
								<a href="/" class="">
									<span data-hover="Heim">Heim</span>
								</a>
							</li>
							<li>
								<a href="/">
									<span data-hover="Staðir">Staðir</span>
								</a>
							</li>
							<li>
								<a href="/">
									<span data-hover="Af hverju náttúrugjald?">Af hverju náttúrugjald?</span>
								</a>
							</li>
							<li>
								<a href="/">
									<span data-hover="Hafðu samband">Hafðu samband</span>
								</a>
							</li>
							<li id="natureLang">
								<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
									<i class="fa fa-globe"></i> <span data-hover="IS">IS</span> <i class="fa fa-caret-down"></i>
								</a>
								<ul id="langList">
									<li>
										<a href="http://naturefee.com/dettifoss" class="langItem lang-en">
											<span data-hover="EN">EN</span>
										</a>
									</li>
									<!-- <li>
										<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
											<span data-hover="GER">GER</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
											<span data-hover="PL">PL</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
											<span data-hover="FR">FR</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
											<span data-hover="CHI">CHI</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/es" class="langItem lang-es">
											<span data-hover="ES">ES</span>
										</a>
									</li> -->
									<!-- <li>
										<a href="http://www.natturugjald.is" class="langItem lang-is">
											<span data-hover="IS">IS</span>
										</a>
									</li> -->
								</ul><!-- #langList -->
							</li><!-- #natureLang -->
						</ul><!-- .navList -->
					</div><!-- #arcade-desktop-nav -->

					<div id="arcade-mobile-nav" class="clearfix">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="navList">
										<li>
											<a href="/" class="">
												<span data-hover="Heim">Heim</span>
											</a>
										</li>
										<li>
											<a href="/">
												<span data-hover="Staðir">Staðir</span>
											</a>
										</li>
										<li>
											<a href="/">
												<span data-hover="Af hverju náttúrugjald?">Af hverju náttúrugjald?</span>
											</a>
										</li>
										<li>
											<a href="/">
												<span data-hover="Hafðu samband">Hafðu samband</span>
											</a>
										</li>
										<li id="natureLang">
											<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
												<i class="fa fa-globe"></i> <span data-hover="IS">IS</span> <i class="fa fa-caret-down"></i>
											</a>
											<ul id="langList">
												<li>
													<a href="/en/dettifoss" class="langItem lang-en">
														<span data-hover="EN">EN</span>
													</a>
												</li>
												<!-- <li>
													<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
														<span data-hover="GER">GER</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
														<span data-hover="PL">PL</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
														<span data-hover="FR">FR</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
														<span data-hover="CHI">CHI</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/es" class="langItem lang-es">
														<span data-hover="ES">ES</span>
													</a>
												</li> -->
												<!-- <li>
													<a href="http://www.natturugjald.is" class="langItem lang-is">
														<span data-hover="IS">IS</span>
													</a>
												</li> -->
											</ul><!-- #langList -->
										</li><!-- #natureLang -->
									</ul><!-- .navList -->
								</div><!-- .col-md-12 -->
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- #arcade-mobile-nav -->

				</div><!-- .container -->
			</div><!-- .headerWrapper -->
		</nav><!-- .navbar -->
		
		<!-- start: COVER-->
		<div id="section_04" class="cover">
		</div><!-- #cover -->
		<!-- end: COVER -->

		<!-- start: SECTION 01 -->
		<div id="section_01" class="section section-locations">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<spna>Dettifoss</spna>
							<hr class="right visible-md visible-lg" />
						</h2>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->
				
				<!-- start: SUB PAGE  -->
				<div class="row">
					<div class="col-sm-6 pull-right">
						<img src="assets/img/dettifoss-big.jpg" alt="Leirhnjúkur - Nature Fee" />
					</div><!-- .col-sm-6 -->
					<div class="col-sm-6 pull-left">
							<p class="subPageText">Stórkostlegt umhverfi Jökulsárgljúfra er mótað af vatni, eldum og ís. Gífurleg hamfarahlaup eru talin hafa myndað og mótað gljúfur, gil, klappir og byrgi. Frægast þeirra er Ásbyrgi. Vatnajökulsþjóðgarður er helsta aðdráttarafl ferðafólks á svæðinu með öllum sínum náttúruperlum, svo sem Ásbyrgi, Hljóðaklettum, Svínadalskvosinni, Hólmatungum og Dettifossi.</p>

							<p class="subPageText">Landslag við þjónustusvæðið er frekar einsleitt og ber ríkan svip af átökum Jökulsár við landið þegar hún hefur farið hamförum og engu eirt. Berar klappir eru áberandi og stórgrýti sem hefur skolast með flóðinu og mótast af veltingnum um klappirnar.  Stórbrotið landslag einsleitt grátt og ber merki flóðanna sem fóru yfir í árdaga. Foksandur er víða og landið er þurrt þar sem úrkoma er í minna lagi. Þrátt fyrir mikinn uppblástur eru víða gróðurlendi, lyng og víðiheiðar og melalönd á sandsvæðunum. Í skjóli hamra og kletta þrífst þó nokkuð fjölbreytt samspil gróðurs og dýralífs.</p>

							<p class="subPageText">Dettifoss er við syðsta hluta Vatnajökulsþjóðgarðs.  Dettifoss er talinn öflugasti foss Evrópu.  Hann er 44-45 metra hár og um 100 metra breiður. Hann er mestur í röð tilkomumikillar fossaraðar sem nær niður í Hólmatungur.  Ofan hans er Selfoss, 10 m hár, og neðan hans er Hafragilsfoss, 27 m hár. Hér er um að ræða fossaröð sem á sér fáa líka á jörðinni. Norðan við Hafragilsfoss skera gljúfrin gígaröð sem heitir Randarhólar. Þar má sjá þverskurð af aðfærsluæð gosgíg. Hvorum megin sem komið er að fossinum, verður að fara með gát.</p>

							<p class="subPageText">Hugmyndir voru uppi um virkjun vatnsaflsins í gljúfrunum, en þær strönduðu á því að hraunlögin eru of gropin til að halda vatni í uppistöðulóni.</p>

							<p class="subPageText">Jökulsá á Fjöllum kemur undan norðanverðum Vatnajökli og fellur til sjávar í Öxarfirði. Við hálendisbrúnina lækkar landið og áin steypist í fossum eftir gljúfrinu sem hún hefur mótað um aldir og eru við hana kennd. Jökulsárgljúfur eru ein stærstu og hrikalegustu árgljúfur á Íslandi, um 25 km löng, ½ km á breidd og dýptin víða um eða yfir 100 m.</p>

							<a href="/" class="btn btn-success btn-lg"><i class="fa fa-chevron-left"></i> Til baka</a>
					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<!-- end: SUB PAGE  -->

			</div><!-- .container -->
		</div><!-- #section_01 -->
		<!-- end: SECTION 01 -->
		<!-- start: FOOTER -->
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="footerInner">
								<span class="footerSep">&nbsp;</span>
								@lang('navbar.Footerline')
								<span class="footerSep">&nbsp;</span> @lang('navbar.Photos')
							</div><!-- .footerInner -->
						</div><!-- .col-sm-12 -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- #footer -->
			<!-- end: FOOTER -->
		
		<!-- start: MEDIA QUERY TESTER -->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mediaQuery">
						MediaQuery!
					</div><!-- .mediaQuery -->
				</div><!-- .col-sm-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
		<!-- end: MEDIA QUERY TESTER -->
		
		<!-- start: TO THE TOP -->
		<a href="" class="toTheTop" title="Fara efst">
			<i class="fa fa-chevron-up"></i>
		</a>
		<!-- end: TO THE TOP -->


		<!-- javascript
		================================================== -->
		<script src="assets/js/jquery1102.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<script src="assets/js/denta-scrolltop.js"></script>
		<script src="assets/js/jquery-scrollto.js"></script>
		<script src="assets/js/naturefee.js"></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-46042256-1', 'bgunnarsson.com');
			ga('send', 'pageview');
		</script>

	</body>
</html>
