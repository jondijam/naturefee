<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="Icelandic Nature - Nature Fee">
		<meta name="keywords" content="Nature, Icelandic Nature, Nature Iceland, Iceland">
		<meta name="author" content="B. Gunnarsson">

		<title>Dettifoss - Locations - Nature Fee</title>

		<!-- Bootstrap core CSS -->
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

		<style type="text/css">
			body {
				padding-top: 65px;
			}
			.grid .test {
				background: #ddd;
			}
			a {
				color: #6a5a8c;
			}
			a:hover {
				color: #6a5a8c;
			}
		</style>
		<!-- <link href="/assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

		<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="/assets/css/animate.css" rel="stylesheet">
		<link href="/assets/css/naturefee.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="/assets/js/html5shiv.js"></script>
		<![endif]-->

	</head>

	<body class="subPage lang-secondary">

		<nav id="header" class="navbar navbar-fixed-top" role="navigation">
			<div class="headerWrapper">
				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<h1 class="logoH1">
							<a href="/" id="logo" class="navbar-brand" title="Nature Fee">
								<img src="/assets/img/logo.png" alt="Nature Fee" />
							</a><!-- .navbar-brand -->
						</h1>
						<a href="" id="mobile-nav-trigger" class="pull-right">
							<i class="fa fa-bars"></i>
						</a><!-- #mobile-nav-trigger -->
					</div><!-- .navbar-header -->

					<div id="arcade-desktop-nav" class="arcadeNav pull-right clearfix">
						<ul class="navList">
							<li>
								<a href="index.html">
									<span data-hover="Home">Home</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Locations">Locations</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Why Nature Fee?">Why Nature Fee?</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Contact">Contact</span>
								</a>
							</li>
							<li id="natureLang">
								<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
									<i class="fa fa-globe"></i> <span data-hover="EN">EN</span> <i class="fa fa-caret-down"></i>
								</a>
								<ul id="langList">
									<!-- <li>
										<a href="http://www.naturefee.com/en" class="langItem lang-en">
											<span data-hover="EN">EN</span>
										</a>
									</li> -->
									<li>
										<a href="http://natturugjald.is/dettifoss" class="langItem lang-is">
											<span data-hover="IS">IS</span>
										</a>
									</li>
									<!-- <li>
										<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
											<span data-hover="GER">GER</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
											<span data-hover="PL">PL</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
											<span data-hover="FR">FR</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
											<span data-hover="CHI">CHI</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/es" class="langItem lang-es">
											<span data-hover="ES">ES</span>
										</a>
									</li> -->
								</ul><!-- #langList -->
							</li><!-- #natureLang -->
						</ul><!-- .navList -->
					</div><!-- #arcade-desktop-nav -->

					<div id="arcade-mobile-nav" class="clearfix">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="navList">
										<li>
											<a href="index.html">
												<span data-hover="Home">Home</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Locations">Locations</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Why Nature Fee?">Why Nature Fee?</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Contact">Contact</span>
											</a>
										</li>
										<li id="natureLang">
											<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
												<i class="fa fa-globe"></i> <span data-hover="EN">EN</span> <i class="fa fa-caret-down"></i>
											</a>
											<ul id="langList">
												<!-- <li>
													<a href="http://www.naturefee.com/en" class="langItem lang-en">
														<span data-hover="EN">EN</span>
													</a>
												</li> -->
												<li>
													<a href="http:://natturugjald.is/dettifoss" class="langItem lang-is">
														<span data-hover="IS">IS</span>
													</a>
												</li>
												<!-- <li>
													<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
														<span data-hover="GER">GER</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
														<span data-hover="PL">PL</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
														<span data-hover="FR">FR</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
														<span data-hover="CHI">CHI</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/es" class="langItem lang-es">
														<span data-hover="ES">ES</span>
													</a>
												</li> -->
											</ul><!-- #langList -->
										</li><!-- #natureLang -->
									</ul><!-- .navList -->
								</div><!-- .col-md-12 -->
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- #arcade-mobile-nav -->

				</div><!-- .container -->
			</div><!-- .headerWrapper -->
		</nav><!-- #header -->
		
		<!-- start: COVER-->
		<div id="section_04" class="cover">
		</div><!-- #cover -->
		<!-- end: COVER -->

		<!-- start: SECTION 01 -->
		<div id="section_01" class="section section-locations">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<spna>Dettifoss</spna>
							<hr class="right visible-md visible-lg" />
						</h2>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->
				
				<!-- start: SUB PAGE  -->
				<div class="row">
					<div class="col-sm-6 pull-right">
						<img src="/assets/img/dettifoss-big.jpg" alt="Leirhnjúkur - Nature Fee" />
					</div><!-- .col-sm-6 -->
					<div class="col-sm-6 pull-left">
							<p class="subPageText">The magnificent environment of Jökulsá canyon is shaped by water, fire and ice. Huge, catastrophic floods are believed to have formed and shaped the canyon, and its gullies, low flat rocks and shelters. The most famous of these is Ásbyrgi. Vatnajökull National Park is the main tourist attraction in the region with all its natural wonders, such as Ásbyrgi, Hljóðaklettum, Svínadalskvosinni, Hólmatungum and Dettifoss.</p>

							<p class="subPageText">The landscape around the service area is fairly uniform and demonstrates the powerful effects of Jökulsá's battle with the earth at its most wild and merciless. It is dominated by bare, flat rocks and large boulders that have been washed down by flooding and shaped by rolling among the rocks. The spectacular scenery is uniformly grey and bears the marks of the flooding that ran through it in previous times. Desert sand is widespread and the land is dry where there is less precipitation. Despite the amount of wind erosion there is widespread vegetation among the sandy areas, including heather, willow tree moors, as well as gravel plains. Quite a varied interplay of vegetation and wildlife thrives in the shelter of the rocks and cliffs.</p>

							<p class="subPageText">Dettifoss is in the southernmost part of the Vatnajökull National Park. Considered to be the most powerful waterfall in Europe, Dettifoss is 44–45 metres high and 100 metres wide. It is the greatest in a series of dramatic waterfalls reaching down into Hólmatungur. Above it is Selfoss, which is 10 metres high, and below it is Hafragilsfoss, which is 27 metres high. This is a range of waterfalls of which there are few on earth. North of Hafragilsfoss, a row of craters called Randarhólar cuts across the canyon. There it is possible to see a cross–section of an erupting crater. There is a waterfall on either side, where it is important to go carefully.</p>

							<p class="subPageText">There was an idea to harness the power of the water in the canyon, but it failed because the lava layers are too porous to contain water in a reservoir.</p>

							<p class="subPageText">The Jökulsá á Fjöllum river emerges from beneath the northern part of the Vatnajökull glacier and drops into the sea at Öxarfjörður. </p>

							<p class="subPageText">By the edge of the plateau the land drops away and the river cascades down the canyon in waterfalls that have formed over the centuries and that are named after it. The Jökulsá canyon is one of the largest and most awe–inspiring canyons in Iceland – around 25 kilometres long, ½ kilometre wide and with a depth of around or above 100 metres.</p>

							<a href="/" class="btn btn-success btn-lg"><i class="fa fa-chevron-left"></i> Back</a>
					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<!-- end: SUB PAGE  -->

			</div><!-- .container -->
		</div><!-- #section_01 -->
		<!-- end: SECTION 01 -->

		<!-- start: FOOTER -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="footerInner">
							<span class="footerSep">&nbsp;</span>
							@lang('navbar.Footerline')
							<span class="footerSep">&nbsp;</span> @lang('navbar.Photos')
						</div><!-- .footerInner -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- #footer -->
		<!-- end: FOOTER -->
		
		<!-- start: MEDIA QUERY TESTER -->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mediaQuery">
						MediaQuery!
					</div><!-- .mediaQuery -->
				</div><!-- .col-sm-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
		<!-- end: MEDIA QUERY TESTER -->
		
		<!-- start: TO THE TOP -->
		<a href="" class="toTheTop" title="Fara efst">
			<i class="fa fa-chevron-up"></i>
		</a>
		<!-- end: TO THE TOP -->


		<!-- javascript
		================================================== -->
		<script src="/assets/js/jquery1102.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<script src="/assets/js/denta-scrolltop.js"></script>
		<script src="/assets/js/jquery-scrollto.js"></script>
		<script src="/assets/js/naturefee.js"></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-46042256-1', 'bgunnarsson.com');
			ga('send', 'pageview');
		</script>

	</body>
</html>
