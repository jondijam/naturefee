<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="Icelandic Nature - Nature Fee">
		<meta name="keywords" content="Nature, Icelandic Nature, Nature Iceland, Iceland">
		<meta name="author" content="B. Gunnarsson">

		<title>Leirhnjúkur - Locations - Nature Fee</title>

		<!-- Bootstrap core CSS -->
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

		<style type="text/css">
			body {
				padding-top: 65px;
			}
			.grid .test {
				background: #ddd;
			}
			a {
				color: #6a5a8c;
			}
			a:hover {
				color: #6a5a8c;
			}
		</style>
		<!-- <link href="/assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

		<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="/assets/css/animate.css" rel="stylesheet">
		<link href="/assets/css/naturefee.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="/assets/js/html5shiv.js"></script>
		<![endif]-->

	</head>

	<body class="subPage lang-secondary">

		<nav id="header" class="navbar navbar-fixed-top" role="navigation">
			<div class="headerWrapper">
				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<h1 class="logoH1">
							<a href="/" id="logo" class="navbar-brand logoToTop" title="Nature Fee">
								<img src="/assets/img/logo.png" alt="Nature Fee" />
							</a><!-- .navbar-brand -->
						</h1>
						<a href="" id="mobile-nav-trigger" class="pull-right">
							<i class="fa fa-bars"></i>
						</a><!-- #mobile-nav-trigger -->
					</div><!-- .navbar-header -->

					<div id="arcade-desktop-nav" class="arcadeNav pull-right clearfix">
						<ul class="navList">
							<li>
								<a href="index.html">
									<span data-hover="Home">Home</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Locations">Locations</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Why Nature Fee?">Why Nature Fee?</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Contact">Contact</span>
								</a>
							</li>
							<li id="natureLang">
								<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
									<i class="fa fa-globe"></i> <span data-hover="EN">EN</span> <i class="fa fa-caret-down"></i>
								</a>
								<ul id="langList">
									<!-- <li>
										<a href="http://www.naturefee.com/en" class="langItem lang-en">
											<span data-hover="EN">EN</span>
										</a>
									</li> -->
									<li>
										<a href="http://natturugjald.is/leirhnjukur" class="langItem lang-is">
											<span data-hover="IS">IS</span>
										</a>
									</li>
									<!-- <li>
										<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
											<span data-hover="GER">GER</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
											<span data-hover="PL">PL</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
											<span data-hover="FR">FR</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
											<span data-hover="CHI">CHI</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/es" class="langItem lang-es">
											<span data-hover="ES">ES</span>
										</a>
									</li> -->
								</ul><!-- #langList -->
							</li><!-- #natureLang -->
						</ul><!-- .navList -->
					</div><!-- #arcade-desktop-nav -->

					<div id="arcade-mobile-nav" class="clearfix">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="navList">
										<li>
											<a href="index.html">
												<span data-hover="Home">Home</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Locations">Locations</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Why Nature Fee?">Why Nature Fee?</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Contact">Contact</span>
											</a>
										</li>
										<li id="natureLang">
											<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
												<i class="fa fa-globe"></i> <span data-hover="EN">EN</span> <i class="fa fa-caret-down"></i>
											</a>
											<ul id="langList">
												<!-- <li>
													<a href="http://www.naturefee.com/en" class="langItem lang-en">
														<span data-hover="EN">EN</span>
													</a>
												</li> -->
												<li>
													<a href="http://www.natturugjald.is" class="langItem lang-is">
														<span data-hover="IS">IS</span>
													</a>
												</li>
												<!-- <li>
													<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
														<span data-hover="GER">GER</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
														<span data-hover="PL">PL</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
														<span data-hover="FR">FR</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
														<span data-hover="CHI">CHI</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/es" class="langItem lang-es">
														<span data-hover="ES">ES</span>
													</a>
												</li> -->
											</ul><!-- #langList -->
										</li><!-- #natureLang -->
									</ul><!-- .navList -->
								</div><!-- .col-md-12 -->
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- #arcade-mobile-nav -->

				</div><!-- .container -->
			</div><!-- .headerWrapper -->
		</nav><!-- #header -->
		
		<!-- start: COVER-->
		<div id="section_04" class="cover">
		</div><!-- #cover -->
		<!-- end: COVER -->

		<!-- start: SECTION 01 -->
		<div id="section_01" class="section section-leirhnjukur">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<spna>Leirhnjúkur-Krafla</spna>
							<hr class="right visible-md visible-lg" />
						</h2>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->
				
				<!-- start: SUB PAGE  -->
				<div class="row">
					<div class="col-sm-6 pull-right">
						<img src="/assets/img/leirhnjukur-big.jpg" alt="Leirhnjúkur - Nature Fee" />
					</div><!-- .col-sm-6 -->
					<div class="col-sm-6 pull-left">
							<p class="subPageText" data-paragraph="1">The Krafla Fires were a series of fissure eruptions and magma flows which occurred during the period of 20 December 1975 to 18 September 1984. The fissures reach from Leirhnjúki in the middle of the Krafla caldera, north to Gjástykki. They are around 11 kilometres long, of which 7.5 kilometres are north of the caldera. The lava forms an area of around 35 km². At the same time the Krafla power plant was being built, using drills to reach the steam in order to power the plant..</p>

							<p class="subPageText">Krafla is a major volcano with a caldera, which is mainly filled with younger formations. In the crust beneath Leirhnjúkur there is a magma chamber at a depth of about three miles where alkaline magma accumulates. Convulsions pressed the magma along cracks in the earth's crust, sometimes to the south and sometimes to the north, and at times so that it was forced up to the surface with eruptions. The magma reached the surface a total of nine times during the Krafla fires:</p>

							<ul class="locationTextList">
								<li>1975, 1st eruption: 20 to 21 December</li>
								<li>1977, 2nd eruption: 27 to 29 April</li>
								<li>1977, 3rd eruption: 8 to 9 September</li>
								<li>1980, 4th eruption: 16 March</li>
								<li>1980, 5th eruption: 10 to 18 July</li>
								<li>1980, 6th eruption: 18 to 23 October</li>
								<li>1981, 7th eruption: 30 January to 4 February</li>
								<li>1981, 8th eruption: 18 to 23 November</li>
								<li>1984, 9th eruption: 4 to 18 September</li>
							</ul>

							<p class="subPageText">The Krafla Fires were similar to the Mývatn Fires, from 1724 to 1729. There the eruptions remained in the caldera – except for one cycle where there was an eruption to the south of it, where where some lava emerged to the west of Námafjall. The lava from Krafla flowed down into an inhabited area and destroyed two towns.</p>

							<p class="subPageText">The Krafla Fires began with a small volcanic eruption by Leihnjúk. The result was a wave of earthquakes that caused fissure movements and subsidence in Kelduhverfi .</p>

							<p class="subPageText">Over the next nine years there were twenty different cycles of differing sizes in the volcanic system. Large amounts of magma were crammed into a section of fissure swarm, just over 70 kilometres long, and the land slid several metres – from Jökulsársandar in the north, south to Hverfell. The drift reached a maximum of 9 metres at the northern perimeter of the caldera.</p>

							<p class="subPageText">The Krafla eruption provided a unique opportunity to research continental drift in volcanic systems on the plate boundary. This generated a great deal of knowledge about volcanic activity and continental drift.</p>

							<p class="subPageText">There are powerful geothermal areas in the caldera of Krafla and Námafjall, and magma intrusions add fuel to the fire. Geothermal energy has been harnessed to produce electricity in the Krafla power station and at Bjarnarflagi.</p>

							<a href="/" class="btn btn-success btn-lg"><i class="fa fa-chevron-left"></i> Back</a>
					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<!-- end: SUB PAGE  -->

			</div><!-- .container -->
		</div><!-- #section_01 -->
		<!-- end: SECTION 01 -->

		<!-- start: FOOTER -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="footerInner">
							<span class="footerSep">&nbsp;</span>
							@lang('navbar.Footerline')
							<span class="footerSep">&nbsp;</span> @lang('navbar.Photos')
						</div><!-- .footerInner -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- #footer -->
		<!-- end: FOOTER -->

		<!-- start: MEDIA QUERY TESTER -->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mediaQuery">
						MediaQuery!
					</div><!-- .mediaQuery -->
				</div><!-- .col-sm-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
		<!-- end: MEDIA QUERY TESTER -->
		
		<!-- start: TO THE TOP -->
		<a href="" class="toTheTop" title="Fara efst">
			<i class="fa fa-chevron-up"></i>
		</a>
		<!-- end: TO THE TOP -->


		<!-- javascript
		================================================== -->
		<script src="/assets/js/jquery1102.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<script src="/assets/js/denta-scrolltop.js"></script>
		<script src="/assets/js/jquery-scrollto.js"></script>
		<script src="/assets/js/naturefee.js"></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-46042256-1', 'bgunnarsson.com');
			ga('send', 'pageview');
		</script>

	</body>
</html>
