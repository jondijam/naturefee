<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="Icelandic Nature - Nature Fee">
		<meta name="keywords" content="Nature, Icelandic Nature, Nature Iceland, Iceland">
		<meta name="author" content="B. Gunnarsson">

		<title>Hverir - Locations - Nature Fee</title>

		<!-- Bootstrap core CSS -->
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

		<style type="text/css">
			body {
				padding-top: 65px;
			}
			.grid .test {
				background: #ddd;
			}
			a {
				color: #6a5a8c;
			}
			a:hover {
				color: #6a5a8c;
			}
		</style>
		<!-- <link href="/assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

		<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="/assets/css/animate.css" rel="stylesheet">
		<link href="/assets/css/naturefee.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="/assets/js/html5shiv.js"></script>
		<![endif]-->

	</head>

	<body class="subPage lang-secondary">

		<nav id="header" class="navbar navbar-fixed-top" role="navigation">
			<div class="headerWrapper">
				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<h1 class="logoH1">
							<a href="/" id="logo" class="navbar-brand" title="Nature Fee">
								<img src="/assets/img/logo.png" alt="Nature Fee" />
							</a><!-- .navbar-brand -->
						</h1>
						<a href="" id="mobile-nav-trigger" class="pull-right">
							<i class="fa fa-bars"></i>
						</a><!-- #mobile-nav-trigger -->
					</div><!-- .navbar-header -->

					<div id="arcade-desktop-nav" class="arcadeNav pull-right clearfix">
						<ul class="navList">
							<li>
								<a href="index.html">
									<span data-hover="Home">Home</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Locations">Locations</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Why Nature Fee?">Why Nature Fee?</span>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span data-hover="Contact">Contact</span>
								</a>
							</li>
							<li id="natureLang">
								<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
									<i class="fa fa-globe"></i> <span data-hover="EN">EN</span> <i class="fa fa-caret-down"></i>
								</a>
								<ul id="langList">
									<!-- <li>
										<a href="http://www.naturefee.com/en" class="langItem lang-en">
											<span data-hover="EN">EN</span>
										</a>
									</li> -->
									<li>
										<a href="http://www.natturugjald.is/hverir" class="langItem lang-is">
											<span data-hover="IS">IS</span>
										</a>
									</li>
									<!-- <li>
										<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
											<span data-hover="GER">GER</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
											<span data-hover="PL">PL</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
											<span data-hover="FR">FR</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
											<span data-hover="CHI">CHI</span>
										</a>
									</li>
									<li>
										<a href="http://www.naturefee.com/es" class="langItem lang-es">
											<span data-hover="ES">ES</span>
										</a>
									</li> -->
								</ul><!-- #langList -->
							</li><!-- #natureLang -->
						</ul><!-- .navList -->
					</div><!-- #arcade-desktop-nav -->

					<div id="arcade-mobile-nav" class="clearfix">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="navList">
										<li>
											<a href="index.html">
												<span data-hover="Home">Home</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Locations">Locations</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Why Nature Fee?">Why Nature Fee?</span>
											</a>
										</li>
										<li>
											<a href="index.html">
												<span data-hover="Contact">Contact</span>
											</a>
										</li>
										<li id="natureLang">
											<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
												<i class="fa fa-globe"></i> <span data-hover="EN">EN</span> <i class="fa fa-caret-down"></i>
											</a>
											<ul id="langList">
												<!-- <li>
													<a href="http://www.naturefee.com/en" class="langItem lang-en">
														<span data-hover="EN">EN</span>
													</a>
												</li> -->
												<li>
													<a href="http://www.natturugjald.is/hverir" class="langItem lang-is">
														<span data-hover="IS">IS</span>
													</a>
												</li>
												<!-- <li>
													<a href="http://www.naturefee.com/ger" class="langItem lang-ger">
														<span data-hover="GER">GER</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/pl" class="langItem lang-pl">
														<span data-hover="PL">PL</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/fr" class="langItem lang-fr">
														<span data-hover="FR">FR</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/chi" class="langItem lang-chi">
														<span data-hover="CHI">CHI</span>
													</a>
												</li>
												<li>
													<a href="http://www.naturefee.com/es" class="langItem lang-es">
														<span data-hover="ES">ES</span>
													</a>
												</li> -->
											</ul><!-- #langList -->
										</li><!-- #natureLang -->
									</ul><!-- .navList -->
								</div><!-- .col-md-12 -->
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- #arcade-mobile-nav -->

				</div><!-- .container -->
			</div><!-- .headerWrapper -->
		</nav><!-- #header -->
		
		<!-- start: COVER-->
		<div id="section_04" class="cover">
		</div><!-- #cover -->
		<!-- end: COVER -->

		<!-- start: SECTION 01 -->
		<div id="section_01" class="section section-locations">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<spna>Hverir</spna>
							<hr class="right visible-md visible-lg" />
						</h2>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->
				
				<!-- start: SUB PAGE  -->
				<div class="row">
					<div class="col-sm-6 pull-right">
						<img src="/assets/img/hverir-big.jpg" alt="Leirhnjúkur - Nature Fee" />
					</div><!-- .col-sm-6 -->
					<div class="col-sm-6 pull-left">
							<p class="subPageText">The Námafjall mountain area is a high temperature region and one of the most visited geothermal spots in Iceland. It lies along a fissure section which extends north from Öxarfjörður through the Krafla volcano and south from Hverfjall/Hverfell. This fissure lies across the entire Námafjall region, while the main source of the hot springs is on the east of the mountain where the geothermal heat is maintained by magma intrusions from the volcano. The hot spring area east of Námafjall is called Hverir.</p>

							<p class="subPageText">"Here you can see the mud pots in various stages of development. They are thought to emerge because of the steam vents, where the steam contains large amounts of hydrogen sulphide, which dissolves the rock and transforms it into a bubbling, grey-black mud porridge. The clay hot springs are in the lower area, but up the hill, which is drier, steam geysers are dominant."</p>

							<p class="subPageText">In earlier centuries there were many sulphur mines around Námafjall, because sulphur was used for gun powder in the Middle Ages. The Danish king acquired the mines in 1563. They were used from time to time until the mid- 19th century and there are still visible traces of sulphur processing in the region. "The columns of steam on the east side of the lava by Hverir are caused because, in the mid- 20th century, several shallow holes were drilled there and then later rocks were used to hide the openings."</p>

							<a href="/" class="btn btn-success btn-lg"><i class="fa fa-chevron-left"></i> Back</a>
					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<!-- end: SUB PAGE  -->

			</div><!-- .container -->
		</div><!-- #section_01 -->
		<!-- end: SECTION 01 -->

		<!-- start: FOOTER -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="footerInner">
							<span class="footerSep">&nbsp;</span>
							@lang('navbar.Footerline')
							<span class="footerSep">&nbsp;</span> @lang('navbar.Photos')
						</div><!-- .footerInner -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- #footer -->
		<!-- end: FOOTER -->

		<!-- start: MEDIA QUERY TESTER -->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mediaQuery">
						MediaQuery!
					</div><!-- .mediaQuery -->
				</div><!-- .col-sm-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
		<!-- end: MEDIA QUERY TESTER -->
		
		<!-- start: TO THE TOP -->
		<a href="" class="toTheTop" title="Fara efst">
			<i class="fa fa-chevron-up"></i>
		</a>
		<!-- end: TO THE TOP -->


		<!-- javascript
		================================================== -->
		<script src="/assets/js/jquery1102.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<script src="/assets/js/denta-scrolltop.js"></script>
		<script src="/assets/js/jquery-scrollto.js"></script>
		<script src="/assets/js/naturefee.js"></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-46042256-1', 'bgunnarsson.com');
			ga('send', 'pageview');
		</script>

	</body>
</html>
