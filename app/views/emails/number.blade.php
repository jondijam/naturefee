<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Nature fee - ticket</title>
	</head>
	<body>
		<h4>@lang('navbar.ThankYouForTheOrder')</h4>
		<p>
			@lang('navbar.PleaseBringThisTicketYou')
		</p>
		<table>
			<thead>
				<tr>
					<th>
						@lang('navbar.Name')
					</th>
					<th>
						@lang('navbar.Label')
					</th>
					<th>
						@lang('QRCode')
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($activities as $activity)
				<tr>
					<th>
						{{$activity->name}};
					</th>
					<th>
						{{$activity->pivot->label}}
					</th>
					<th>
						{{$activity->pivot->QRCode}}
					</th>
				</tr>
				@endforeach
			</tbody>
		</table>
	</body>
</html>