<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Confirmation</h2>

		<div>
			Please click on to confirm your email: {{ URL::to('email/confirmation', array($confirm_string, $user_id)) }}.
		</div>
	</body>
</html>
