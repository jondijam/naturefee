<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="Icelandic Nature - Nature Fee">
		<meta name="keywords" content="Nature, Icelandic Nature, Nature Iceland, Iceland">
		<meta name="author" content="www.bgunnarsson.com">
		<meta http-equiv='cache-control' content='no-cache'>
		<meta http-equiv='expires' content='0'>
		<meta http-equiv='pragma' content='no-cache'>

		<title>Nature Fee</title>

		<!-- Bootstrap core CSS -->
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

		<style type="text/css">
			body {
				padding-top: 65px;
			}
			.grid .test {
				background: #ddd;
			}
			a {
				color: #6a5a8c;
			}
			a:hover {
				color: #6a5a8c;
			}
		</style>
		<!-- <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

		<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="/assets/css/animate.css" rel="stylesheet">
		<!-- <link href="assets/css/lightbox.css" rel="stylesheet"> -->
		<link href="/assets/css/naturefee.css?version=0.1" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body class="frontpage lang-default">
		<nav id="header" class="navbar navbar-fixed-top" role="navigation">
			<div class="headerWrapper">
				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<h1 class="logoH1">
							<a href="" id="logo" class="navbar-brand logoToTop" title="Nature Fee">
								<img @if($locale == "en") src="/assets/img/logo.png" @else src="/assets/img/logo-is.png" @endif alt="Nature Fee" />
							</a><!-- .navbar-brand -->
						</h1>
						<a href="" id="mobile-nav-trigger" class="pull-right">
							<i class="fa fa-bars"></i>
						</a><!-- #mobile-nav-trigger -->
					</div><!-- .navbar-header -->

					<div id="arcade-desktop-nav" class="arcadeNav pull-right clearfix">
						<ul class="navList">
							<li>
								<a href="" id="navHome" class="navItem" data-section="01">
									<span data-hover="@lang('navbar.Locations')">@lang('navbar.Locations')</span>
								</a>
							</li>
							<li>
								<a href="" id="navAbout" class="navItem" data-section="02">
									<span data-hover="@lang('navbar.WhyNatureFee')?">@lang('navbar.WhyNatureFee')?</span>
								</a>
							</li>
							<li>
								<a href="" id="navContact" class="navItem" data-section="03">
									<span data-hover="@lang('navbar.ContactUs')">@lang('navbar.ContactUs')</span>
								</a>
							</li>
							<li id="natureLang">
								<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
									<i class="fa fa-globe"></i> <span data-hover="{{{strtoupper($locale)}}}">{{{strtoupper($locale)}}}</span> <i class="fa fa-caret-down"></i>
								</a>
								<ul id="langList">
									@foreach(Lang::get( 'navbar.option' ) as $key => $option)
										
										@if($locale != $key)
											<li>
												<a href="@if($locale == "is") http://naturefee.com @else http://natturugjald.is  @endif" class="langItem lang-{{{$key}}}">
													<span data-hover="{{{strtoupper($key)}}}">{{{strtoupper($key)}}}</span>
												</a>
											</li>
										@endif
									@endforeach
									
								</ul>
							</li><!-- #natureLang -->
						</ul><!-- .navList -->
					</div><!-- #arcade-desktop-nav -->

					<div id="arcade-mobile-nav" class="clearfix">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="navList">
										<li>
											<a href="" id="navHome" class="navItem" data-section="01">
												<span data-hover="@lang('navbar.Locations')">@lang('navbar.Locations')</span>
											</a>
										</li>
										<li>
											<a href="" id="navAbout" class="navItem" data-section="02">
												<span data-hover="@lang('navbar.WhyNatureFee')?">@lang('navbar.WhyNatureFee')?</span>
											</a>
										</li>
										<li>
											<a href="" id="navContact" class="navItem" data-section="03">
												<span data-hover="@lang('navbar.ContactUs')">@lang('navbar.ContactUs')</span>
											</a>
										</li>
										<li id="natureLang">
										<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
											<i class="fa fa-globe"></i> <span data-hover="{{{strtoupper($locale)}}}">{{{strtoupper($locale)}}}</span> <i class="fa fa-caret-down"></i>
										</a>
										<ul id="langList">
											@foreach(Lang::get( 'navbar.option' ) as $key => $option)
												
												@if($locale != $key)
													<li>
														<a href="@if($locale == "is") http://naturefee.com @else http://natturugjald.is  @endif" class="langItem lang-{{{$key}}}">
															<span data-hover="{{{strtoupper($key)}}}">{{{strtoupper($key)}}}</span>
														</a>
													</li>
												@endif
											@endforeach
											
										</ul>
									</li><!-- #natureLang -->
									</ul><!-- .navList -->
								</div><!-- .col-md-12 -->
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- #arcade-mobile-nav -->

				</div><!-- .container -->
			</div><!-- .headerWrapper -->
		</nav><!-- .navbar -->

		<!-- start: COVER-->
		<div id="section_04" class="cover">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<div class="coverWhy">
							<h1>@lang('navbar.WhyNatureFee')?</h1>
							<p class="whyParagraph">@lang('navbar.WhyNatureFeeHomeText')
							</p>
							<a href="" id="coverMoreInfo" class="navItem" data-section="02">
								@lang('navbar.MoreInformation') <br />
								<i class="fa fa-chevron-down"></i>
							</a>
						</div><!-- .coverWhy -->

					</div><!-- .col-sm-7 -->
					<!-- start BUY BOX -->
					<div class="col-sm-5">
						<div id="buyBox" class="box borderRadiusTop">
							<h2>@lang('navbar.Buy') <span class="green">@lang('navbar.Now')</span></h2>

							<ul id="buyList">
								<li><h3 class="pickArea">@lang('navbar.ChooseArea'):</h3></li>
								<li class="checkboxin checkboxin01 clearfix" data-locale="{{$locale}}" data-checkbox="03">
									<div class="checkbox emptyCheck pull-left"></div>
									<div class="listText pull-left">Dettifoss <span class="hidden small activity-free" style="color:#e74c3c;">@lang('navbar.FreeOnDettifoss')</span></div>
								</li>
								<li class="checkboxin clearfix" data-locale="{{$locale}}" data-checkbox="02">
									<div class="checkbox emptyCheck pull-left"></div>
									<div class="listText pull-left">Hverir</div>
								</li>
								<li class="checkboxin clearfix" data-locale="{{$locale}}" data-checkbox="01">
									<div class="checkbox emptyCheck pull-left"></div>
									<div class="listText pull-left">Leirhnjúkur-Krafla</div>
								</li>
							</ul>

							<div class="buyEquals clearfix">
								<span class="equalSign pull-left">=</span> 
								<span id="offHolder" class="buyOff pull-right"> - 0</span> <span id="priceHolder" class="buyPrice pull-right">0 ISK</span> 
							</div>
							<p> @lang('navbar.NoNatureFeeUnder18')</p>

							<a href="/buy" id="buyTrigger" data-locale="{{{$locale}}}" class="btn btn-success btn-lg">@lang('navbar.BuyNow')</a>

						</div><!-- #buyBox --
>					</div><!-- .col-sm-5 -->
					<!-- end: BUY BOX -->
				</div>
			</div>
		</div>

		<!-- start: SECTION 01 -->
		<div id="section_01" class="section section-locations">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<span>@lang('navbar.Locations')</span>
							<hr class="right visible-md visible-lg" />
						</h2>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->

				<div class="row">

					<!-- start: LOCATIONS -->
					<div class="col-sm-4">
						<div id="location1" class="location">
							<div class="location-img">
								<a href="/dettifoss" title="Dettifoss - Nature Fee">
									<img src="assets/img/dettifoss.jpg" alt="Dettifoss - Nature Fee" />
									<span class="location-caption">
										@lang('navbar.NoNATUREFEEAtDettifoss2014')
										
									</span>
								</a>
							</div>
						
							<h3 class="locationTitle">
								<a href="/dettifoss">Dettifoss</a>
							</h3>
							<p class="locationText">@lang('navbar.DettifossLocationSmallText')</p>
							<a href="/dettifoss" class="btn btn-success btn-lg">@lang('navbar.MoreInformation')</a>
						</div><!-- .location -->
					</div><!-- .col-sm-4 -->
					<div class="col-sm-4">
						<div id="location2" class="location">
							<a href="/hverir" title="Hverir - Nature Fee">
								<img src="assets/img/hverir.jpg" alt="Hverir - Nature Fee" />
							</a>
							<h3 class="locationTitle">
								<a href="/hverir">Hverir</a>
							</h3>
							<p class="locationText">@lang('navbar.HverirLocationSmallText') </p>
							<a href="/hverir" class="btn btn-success btn-lg">@lang('navbar.MoreInformation')</a>
						</div><!-- .location -->
					</div><!-- .col-sm-4 -->
					<div class="col-sm-4">
						<div id="location3" class="location">
							<a href="/leirhnjukur" title="Leirhnjúkur - Nature Fee">
								<img src="/assets/img/leirhnjukur.jpg" alt="Leirhnjúkur - Nature Fee" />
							</a>
							<h3 class="locationTitle">
								<a href="/leirhnjukur">Leirhnjúkur-Krafla</a>
							</h3>
							<p class="locationText">@lang('navbar.LeirhnjukurLocationSmallText')</p>
							<a href="/leirhnjukur" class="btn btn-success btn-lg">@lang('navbar.MoreInformation')</a>
						</div><!-- .location -->
					</div><!-- .col-sm-4 -->
					<!-- end: LOCATIONS -->

				</div><!-- .row -->
				
				<div class="row" style="margin-top: 29px;">
					<div class="col-sm-6 col-sm-offset-3">
						<div id="location3" class="location">
							
							<img src="/assets/img/random/cover1.jpg" alt="Víti" />
					
							<h3 class="locationTitle" style="text-align:center;">
								<a href="#">Víti</a>
							</h3>
							<!-- <p class="locationText">Innifalið í nátturuverndargjaldinu  er skoðun á Víti</p> -->
							<!-- <a href="leirhnjukur.html" class="btn btn-success btn-lg">Frekari upplýsingar</a> -->
						</div><!-- .location -->
					</div><!-- .col-sm-4 -->
				</div><!-- .row -->

			</div><!-- .container -->
		</div><!-- #section_01 -->
		<!-- end: SECTION 01 -->

		<!-- start: SECTION 02 -->
		<div id="section_02" class="section section-why sectionDark">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<!-- start: SECTION TITLE -->
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<span>@lang('navbar.WhyNatureFee')?</span>
							<hr class="right visible-md visible-lg" />
						</h2>
						<!-- end: SECTION TITLE -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->

				<!-- start: WHY FEE -->
				<div class="row">
					<div class="col-md-6">
						@lang('navbar.WhyNatureFeeParagraphs')			
					</div><!-- .col-sm-6 -->
					<div class="col-md-6 imgRow">
						<div class="row">
							<div class="col-sm-6">
								<a href="assets/img/whyimg1-big.jpg" data-lightbox="locations-1">
									<img src="assets/img/whyimg1.jpg" />
								</a>
							</div><!-- .col-sm-6 -->
							<div class="col-sm-6">
								<a href="assets/img/whyimg2-big.jpg" data-lightbox="locations-2">
									<img src="assets/img/whyimg2.jpg" />
								</a>
							</div><!-- .col-sm-6 -->
						</div><!-- .row -->
						<div class="row whyImgSecondRow">
							<div class="col-sm-6">
								<a href="assets/img/whyimg3-big.jpg" data-lightbox="locations-3">
									<img src="assets/img/whyimg3.jpg" />
								</a>

							</div><!-- .col-sm-6 -->
							<div class="col-sm-6">
								<a href="assets/img/whyimg4-big.jpg" data-lightbox="locations-4">
									<img src="assets/img/whyimg4.jpg" />
								</a>
							</div><!-- .col-sm-6 -->
						</div><!-- .row -->
						<div class="row" style="margin-top: 30px;">
							<div class="col-sm-12" style="text-align: center;">
								<p class="whyText">
									<a href="http://www.volcanic-springs.com/" class="btn btn-success btn-lg" target="_blank">@lang('navbar.SeeMorePictures')</a>
								</p>
							</div><!-- .col-sm-12 -->
						</div><!-- .row -->
					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<!-- end: WHY FEE -->

			</div><!-- .container -->
		</div><!-- #section_02 -->
		<!-- end: SECTION 02 -->

		<!-- start: SECTION 03 -->
		<div id="section_03" class="section section-contact">
			<div class="container">

				<!-- start: SECTION TITLE -->
				<div class="row">
					<div class="col-sm-12">
						<!-- start: SECTION TITLE -->
						<h2 class="sectionTitle">
							<hr class="left visible-md visible-lg" />
							<span>@if($locale == "en") 
								Contact us  
								@else 
								Hafa samband 
								@endif
							</span>
							<hr class="right visible-md visible-lg" />
						</h2>
						<!-- end: SECTION TITLE -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- end: SECTION TITLE -->
				
				<!-- start: CONTACT FORM -->
				<form action="/contact" id="sambandForm">
					<div class="row">
						<div class="col-sm-12">
							<div id="errormessages" class="error"></div><!-- #errormessages -->
						</div><!-- .col-sm-12 -->
					</div><!-- .row -->
					<div class="row">
						<div class="col-sm-6 upper-col-sm-6">
							<input type="text" id="name" name="name" placeholder="@lang('navbar.Name')" style="width:100%;" />
						</div><!-- .col-sm-6 -->
						<div class="col-sm-6">
							<input type="text" id="email" name="email" placeholder="E-mail" style="width:100%;" />
						</div><!-- .col-sm-6 -->
					</div><!-- .row -->
					<div class="row">
						<div class="col-sm-12">
							<textarea name="" id="message" name="message" cols="30" rows="10" placeholder="@lang('navbar.Message')" style="width:100%;"></textarea>
						</div><!-- .col-sm-12 -->
					</div><!-- .row -->
					<div class="row" style="margin-top: 20px;">
						<div class="col-sm-12">
							<div class="buttonRow clearfix">
								<input type="submit" id="sender" class="btn btn-success btn-lg pull-right" value="@lang('navbar.SendMessage')" />
								<input type="text" name="robotest" id="robotest" class="robotest" style="visibility:hidden;" /> 
							</div><!-- .buttonRow -->
						</div><!-- .col-sm-12 -->
					</div><!-- .row -->
				</form><!-- #contactForm -->

				<!-- END: CONTACT FORM -->
			</div><!-- .container -->
		</div><!-- #section_03 -->
		<!-- end: SECTION 03 -->

		<!-- start: FOOTER -->
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="footerInner">
								<span class="footerSep">&nbsp;</span>
								@lang('navbar.Footerline')
								<span class="footerSep">&nbsp;</span> @lang('navbar.Photos')
							</div><!-- .footerInner -->
						</div><!-- .col-sm-12 -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- #footer -->
			<!-- end: FOOTER -->

		<!-- javascript
		================================================== -->
		<script src="/assets/js/jquery1102.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<script src="/assets/js/denta-scrolltop.js"></script>
		<script src="/assets/js/jquery-scrollto.js"></script>
		<!-- <script src="assets/js/lightbox-2.6.min.js"></script> -->
		<script src="/assets/js/naturefee.js"></script>

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-49380319-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	</body>
</html>