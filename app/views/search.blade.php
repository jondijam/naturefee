<div class="">
	<ul class="list-group">
	@if(count($profiles) > 0)
		
		@foreach($profiles as $profile)
			@if($profile->active == 1)
			<li class="list-group-item"><a href="/profile/{{{$profile->profile_id}}}">{{{$profile->name}}} {{{$profile->address}}} {{{$profile->zip}}} {{{$profile->city}}}</a></li>
			@endif
		@endforeach
	@endif
	@if(count($phone_numbers) > 0)
		@foreach($phone_numbers as $phone_number)
			@if($phone_number->$profile->active == 1)
			<li class="list-group-item"><a href="/profile/{{{$phone_number->profile->profile_id}}}">{{{$phone_number->profile->name}}} {{{$phone_number->profile->address}}} {{{$phone_number->profile->zip}}} {{{$phone_number->profile->city}}}</a></li>
			@endif
		@endforeach
	@endif
	@if(count($organisations) > 0)
		@foreach($organisations as $organisation)
			@if($active[$organisation->organisation_id] == 1)
			<li class="list-group-item"><a href="/organisation/{{$organisation->organisation_id}}">{{$organisation->name}}</a></li>
			@endif
		@endforeach
	@endif
	</ul>
</div>