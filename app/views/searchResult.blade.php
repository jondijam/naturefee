<!DOCTYPE html>
	<html lang="en">
		<head>
			@include('head')
		</head>
		<body>
			<div class="navbar navbar-default">
		      <div class="container">
		        <div class="navbar-header">
		          <a href="/" class="navbar-brand">looll.is</a>
		          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		        </div>
		        <div class="navbar-collapse collapse" id="navbar-main">
		          @if($loggedin)
		            <ul class="nav navbar-nav navbar-right">
		              @if(count($contact_organisations) > 0)
		              <li>
		                <a href="/organisation">@lang('looll.Organisation')</a>
		              </li>
		              @endif
		              <li>
		                <a href="/profile">@lang('looll.MyProfile')</a>
		              </li>
		               <li>
		                <a href="/account">@lang('looll.Account')</a>
		              </li>
		              <li>
		                <a href="/logout">@lang('looll.Logout')</a>
		              </li>
		            </ul>
		          @else
		          <ul class="nav navbar-nav navbar-right">
		            <li>
		              <a href="/register">@lang('looll.Registration')</a>
		            </li>
		            <li>
		              <a href="/login">@lang('looll.Login')</a>
		            </li>
		          </ul>
		          @endif
		          
		        </div>
		      </div>
		    </div>
		    <div class="container">
		    	<div class="row">
		    		<div class="col-sm-3">
			        </div>
			        <div class="col-sm-6">
			        	<h1>Leitarniðurstöður</h1>
			        	@if(count($profiles) > 0 || count($phone_numbers) > 0 || count($organisations) > 0)
				        	@foreach($profiles as $profile)
				        	@if($profile->active == 1)
				        	<div class="panel panel-default">
							  <div class="panel-body">
							     <div class="panel-body" >
							    	<a href="/profile/{{$profile->profile_id}}">{{{$profile->name}}} </a>
							    	<p>{{{$profile->address}}} {{{$profile->zip}}} {{{$profile->city}}}</p>
							  	</div>
							  </div>
							</div>
							@endif
				        	@endforeach
				        	@foreach($phone_numbers as $phone_number)
				        	@if($phone_number->profile->active == 1)
				        	<div class="panel panel-default">
							  <div class="panel-body" >
							    	<a href="/profile/{{$phone_number->profile->profile_id}}">{{{$phone_number->profile->name}}} </a>
							    	<p>{{{$phone_number->profile->address}}} {{{$phone_number->profile->zip}}} {{{$phone_number->profile->city}}}</p>
							  </div>
							</div>
							@endif()
				        	@endforeach
				        	@foreach($organisations as $organisation)
				        		@if($active[$organisation->organisation_id] == 1)
					        	<div class="panel panel-default">
								  <div class="panel-body">
									<div class="col-sm-7">
								    	<a href="/organisation/{{$organisation->organisation_id}}">{{{$organisation->name}}}</a>
								    </div>
				
								  </div>
								</div>
								@endif
				        	@endforeach
			        	@endif


			        </div>
		    	</div>
		    </div>
		</body>
	</html>