<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
			<meta name="description" content="Icelandic Nature - Nature Fee">
			<meta name="keywords" content="Nature, Icelandic Nature, Nature Iceland, Iceland">
			<meta name="author" content="B. Gunnarsson">

			<title>Dettifoss - Locations - Nature Fee</title>

			<!-- Bootstrap core CSS -->
			<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

			<style type="text/css">
				body {
					padding-top: 65px;
				}
				.grid .test {
					background: #ddd;
				}
				a {
					color: #6a5a8c;
				}
				a:hover {
					color: #6a5a8c;
				}
			</style>
			<!-- <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

			<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
			<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
			<link href="/assets/css/animate.css" rel="stylesheet">
			<link href="/assets/css/naturefee.css" rel="stylesheet">

			<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
			<!--[if lt IE 9]>
				<script src="assets/js/html5shiv.js"></script>
			<![endif]-->
		</head>
		<body class="subPage">
			<nav id="header" class="navbar navbar-fixed-top" role="navigation">
				<div class="headerWrapper">
				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<h1 class="logoH1">
							<a href="/" id="logo" class="navbar-brand logoToTop" title="Nature Fee">
							
								<img @if($locale == "en") src="/assets/img/logo.png" @else src="/assets/img/logo-is.png" @endif alt="Nature Fee" />
							</a><!-- .navbar-brand -->
						</h1>
						<a href="" id="mobile-nav-trigger" class="pull-right">
							<i class="fa fa-bars"></i>
						</a><!-- #mobile-nav-trigger -->
					</div><!-- .navbar-header -->

					<div id="arcade-desktop-nav" class="arcadeNav pull-right clearfix">
						<ul class="navList">
							<li>
								<a href="/" id="navHome">
									<span data-hover="@lang('navbar.Locations')">@lang('navbar.Locations')</span>
								</a>
							</li>
							<li>
								<a href="/" id="navAbout">
									<span data-hover="@lang('navbar.WhyNatureFee')?">@lang('navbar.WhyNatureFee')?</span>
								</a>
							</li>
							<li>
								<a href="/">
									<span data-hover="@lang('navbar.Contact')">@lang('navbar.Contact')</span>
								</a>
							</li>
							<li id="natureLang">
								<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
									<i class="fa fa-globe"></i> <span data-hover="{{{strtoupper($locale)}}}">{{{strtoupper($locale)}}}</span> <i class="fa fa-caret-down"></i>
								</a>
								<ul id="langList">
									@foreach(Lang::get( 'navbar.option' ) as $key => $option)
										
										@if($locale != $key)
											<li>
												<a href="@if($locale == "is") http://naturefee.com/cart @else http://natturugjald.is/cart  @endif" class="langItem lang-{{{$key}}}">
													<span data-hover="{{{strtoupper($key)}}}">{{{strtoupper($key)}}}</span>
												</a>
											</li>
										@endif
									@endforeach	
								</ul>
							</li><!-- #natureLang -->
						</ul><!-- .navList -->
					</div><!-- #arcade-desktop-nav -->

					<div id="arcade-mobile-nav" class="clearfix">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="navList">
										<li>
											<a href="/">
												<span data-hover="@lang('navbar.Locations')">@lang('navbar.Locations')</span>
											</a>
										</li>
										<li>
											<a href="/">
												<span data-hover="@lang('navbar.WhyNatureFee')?">@lang('navbar.WhyNatureFee')?</span>
											</a>
										</li>
										<li>
											<a href="/">
												<span data-hover="@lang('navbar.Contact')">@lang('navbar.Contact')</span>
											</a>
										</li>
										<li id="natureLang">
											<a href="" id="navLangTrigger" title="Contact us - Nature Fee">
												<i class="fa fa-globe"></i> <span data-hover="{{{strtoupper($locale)}}}">{{{strtoupper($locale)}}}</span> <i class="fa fa-caret-down"></i>
											</a>
											<ul id="langList">
												@foreach(Lang::get( 'navbar.option' ) as $key => $option)
													
													@if($locale != $key)
														<li>
															<a href="/@if($locale == "is"){{{$key}}}@endif" class="langItem lang-{{{$key}}}">
																<span data-hover="{{{strtoupper($key)}}}">{{{strtoupper($key)}}}</span>
															</a>
														</li>
													@endif
												@endforeach
												
											</ul>
										</li><!-- #natureLang -->
									</ul><!-- .navList -->
								</div><!-- .col-md-12 -->
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- #arcade-mobile-nav -->

				</div><!-- .container -->
				</div><!-- .headerWrapper -->
			</nav><!-- .navbar -->
			<!-- start: COVER-->
			<div id="section_04" class="cover">
			</div><!-- #cover -->
			<!-- end: COVER -->
			<!-- start: SECTION 01 -->
			<div id="section_01" class="section section-buy">
				<div class="container">
					<!-- start: SECTION TITLE -->
					<div class="row">
						<div class="col-sm-12">
							<h2 class="sectionTitle">
								<hr class="left visible-md visible-lg" />
								<span>@lang('navbar.Buy') @lang('navbar.Now')</span>
								<hr class="right visible-md visible-lg" />
							</h2>
						</div>
					</div>
					<!-- start: SUB PAGE  -->
					<div class="row">	
						<div class="col-sm-12">
							<table class="table">
								<thead>
									<tr>
										<th>@lang('navbar.Name')</th>
										<th>@lang('navbar.UnitAmount')</th>
										<th>@lang('navbar.Qty')</th>
										<th>@lang('navbar.SubTotal')</th>
										<th>@lang('navbar.Action')</th>
									</tr>
								</thead>
								<tbody id="buyBox" style="text-align:left">
									@foreach($carts as $cart)
										<tr>
											<td>{{{$cart->activity->name}}}</td>
											<td>{{{$cart->activity->unit_amount}}}</td>
											<td><input type="text" id="qty{{$cart->cart_id}}" name="quantity" class="small quantity" value="{{{$cart->qty}}}" /></td>
											<td id="{{{$cart->cart_id}}}subtotal">{{$cart->activity->unit_amount * $cart->qty}}</td>
											<td><a href="/cart/update/{{{$cart->cart_id}}}" data-unit-amount="{{{$cart->activity->unit_amount}}}" data-cart-id="{{{$cart->cart_id}}}"" class="update btn btn-success">@lang('navbar.Update')</a> <a href="/cart/delete/{{{$cart->cart_id}}}" class="btn btn-danger">@lang('navbar.Delete')</a></td>
										</tr>
									@endforeach()
								</tbody>
							</table>
							<a href="/information" class="btn btn-success">@lang('navbar.Information')</a>
						</div>
					</div><!-- .row -->
					<!-- end: SUB PAGE  -->
					
				</div>
			</div><!-- #section_01 -->
			<!-- end: SECTION 01 -->
			
			<!-- start: FOOTER -->
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="footerInner">
								<span class="footerSep">&nbsp;</span>
								@lang('navbar.Footerline')
								<span class="footerSep">&nbsp;</span> @lang('navbar.Photos')
							</div><!-- .footerInner -->
						</div><!-- .col-sm-12 -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- #footer -->
			<!-- end: FOOTER -->


		<!-- javascript
		================================================== -->
		<script src="/assets/js/jquery1102.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<script src="/assets/js/denta-scrolltop.js"></script>
		<script src="/assets/js/jquery-scrollto.js"></script>
		<!-- <script src="assets/js/lightbox-2.6.min.js"></script> -->
		<script src="/assets/js/naturefee.js"></script>
		<script src="/assets/js/cart.js"></script>

		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-49380319-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
		
		</body>
	</html>

