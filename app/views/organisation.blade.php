<!DOCTYPE html>
	<html lang="en">
		<head>
			 @include('head')
		</head>
		<body>
			<div class="navbar navbar-default">
				<div class="container">
					<div class="navbar-header">
						<a href="/" class="navbar-brand">looll.is</a>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
							<span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-main">
						<ul class="nav navbar-nav navbar-right">
							@if(count($organisations) > 0)
							<li class="active">
								<a href="/organisation">@lang('looll.Organisation')</a>
							</li>
							@endif
							<li>
								<a href="/profile">@lang('looll.MyProfile')</a>
							</li>
							<li>
								<a href="/account">@lang('looll.Account')</a>
							</li>
							<li>
								<a href="/logout">@lang('looll.Logout')</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<table class="table">
							<thead>
								@if(count($organisations) > 0)
								<tr>
									<th>
										@lang('looll.Name')
									</th>
									<th>
										@lang('looll.Action')
									</th>
								</tr>
								@endif
							</thead>
							<tbody>
								@if(count($organisations) > 0)
								@foreach($organisations as $organisation)
								<tr>
									<td>
										{{{$organisation->name}}}
									</td>
									<td>
										<a href="/organisation/edit/{{{$organisation->organisation_id}}}" class="btn btn-primary">@lang('looll.Edit')</a>
										<a href="/organisation/delete/{{{$organisation->organisation_id}}}" class="btn btn-danger">@lang('looll.Delete')</a>
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
						<a href="/organisation/edit/0" class="btn btn-primary">@lang('looll.Create')</a>
					</div>
				</div>
			</div>
			@include('footer')
			<script type="text/javascript" src="/js/organisation.js"></script>
		</body>
	</html>