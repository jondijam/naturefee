<!DOCTYPE html>
<html lang="en">
  <head>
    @include('head')
  </head>
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a href="/" class="navbar-brand">looll.is</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          @if($loggedin)
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="/organisation">@lang('looll.Organisation')</a>
              </li>
              <li>
                <a href="/profile">@lang('looll.MyProfile')</a>
              </li>
               <li>
                <a href="/account">@lang('looll.Account')</a>
              </li>
              <li>
                <a href="/logout">@lang('looll.Logout')</a>
              </li>
            </ul>
          @else
            <ul class="nav navbar-nav navbar-right">
            <li>
              <a href="/register">@lang('looll.Registration')</a>
            </li>
            <li>
              <a href="/login">@lang('looll.Login')</a>
            </li>
            </ul>
          @endif
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h1>{{{$name}}}</h1>
          </div>
        </div>
      </div>
      <div class="row">
        @if(count($organisation) > 0)
        <div class="col-sm-3">
          <div class="images">
            @if(count($images) > 0)
              @foreach($images as $image)
                <div class="thumbnail">
                  <img src="{{{$image->src}}}" alt="{{{$image->name}}}" class="img-thumbnail" />
                </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="col-sm-6">
          <div class="col-sm-6">
            <address>
              {{{$address}}}<br>
              {{{$zip}}} {{{$city}}}<br>
              {{{$country}}}<br />
              {{{$email}}}
            </address>
            <h5>Contacts</h5>
            <address>
              @foreach($contacts as $contact)
                <a href="/profile/{{$contact->profile->profile_id}}">{{$contact->profile->name}}</a><br />            
              @endforeach
            </address>
          </div>
          <div class="col-sm-6">
            <div class="well">
              <h4 style="margin-bottom: 0.4em;">@lang('looll.About', array('name'=>$name))</h4>
              <p>{{{$description}}}</p>
            </div>
          </div>
          <div class="col-sm-12">
            <h3>@lang('looll.Announcement')</h3>
            <div class="well">
              <p>{{{$messages}}}</p>
            </div>
          </div>
          <div class="col-sm-12">
            @if($loggedin && $membersStatus == 2)
              @if(count($user_organisations) == 0)
                  <a href="/" id="connect" data-user-id="{{$user_id}}" data-organisation-id="{{$organisation_id}}" class="btn btn-primary pull-right">Connect</a>
              @else
                @foreach($user_organisations as $user_organisation)
                  @if($user_organisation->pivot->owner == 0 || $user_organisation->pivot->owner == 2)
                      <a href="/" id="disConnect" data-organisation-id="{{$organisation_id}}" class="btn btn-danger pull-right">Disconnect</a>
                  @else
                      <a href="/organisation/delete/{{$organisation_id}}" data-organisation-id="{{$organisation_id}}" class="btn btn-danger pull-right">@lang('looll.Delete')</a>
                  @endif
                @endforeach
              
              @endif
            @endif
          </div>
        </div>
        <div class="col-sm-3">
          <div class="sidebar">
            <div class="panel-group" id="accordion">
                  @if($membersStatus == 2)
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            @lang('looll.Members')
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse">
                         <div class="panel-body">
                            <ul class="media-list">
                              @foreach($members as $member)
                                <li><a href="/profile/{{$member->profile->profile_id}}">{{$member->profile->name}}</a></li>
                              @endforeach
                            </ul>
                         </div>
                      </div>
                    </div>
                  @elseif($membersStatus == 3 && count($associatedOrganizations) > 0) 
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                           @lang('looll.AssociatedOrganizations')
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                       <div class="panel-body">
                            <ul class="media-list">
                              @foreach($associatedOrganizations as $organisation)
                                <li><a href="/organisation/{{$organisation->organisation_id}}">{{$organisation->name}}</a></li>
                              @endforeach
                            </ul>
                         </div>
                      </div>
                    </div>
                  @endif
            </div>
          </div>
        </div>
        @else
        <div class="col-sm-12">
          <div class="alert alert-warning">
            <p>@lang('looll.NoOrganisation')</p>
          </div>
        </div>
        @endif
      </div>
    </div>
    @include('footer')
    <script type="text/javascript" src="/js/organisation.js"></script>
  </body>
</html>