<?php 
class Activity extends Eloquent
{
	protected $table = 'activities';
	protected $primaryKey = 'activity_id';
	public function cart()
	{
		return $this->hasMany('Cart');
	}

	public function orders()
	{
		return $this->belongsToMany('Order','orders_activities')->withPivot('qty', 'subtotal','label','QRCode','ValidFrom','ValidTo')->withTimestamps();
	}
}
?>