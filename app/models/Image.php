<?php 
class Image extends Eloquent 
{
	protected $table = 'images';
    protected $primaryKey = 'image_id';

    public function profiles()
    {
    	return $this->belongsToMany('Profile','profiles_have_images')->withTimestamps();
    }

    public function organisations()
    {
    	return $this->belongsToMany('Organisation','organisations_have_images');
    }
}
?>