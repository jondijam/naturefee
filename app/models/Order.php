<?php
class Order extends Eloquent
{
	protected $table = 'orders';
	protected $primaryKey = 'order_id';

	public function activities()
	{
		return $this->belongsToMany('Activity','orders_activities')->withPivot('qty', 'subtotal','label','QRCode')->withTimestamps();
	}

	public function Profile()
	{
		return $this->belongsTo('Profile');
	}

}
?>