<?php 
class Login extends Eloquent 
{

    protected $table = 'Login';
    protected $primaryKey = 'login_id';

    public function user()
    {
    	return $this->belongsTo('User');
    }
}
?>