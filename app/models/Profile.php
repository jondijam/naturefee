<?php 
class Profile extends Eloquent
{
	protected $table = 'profiles';
	protected $primaryKey = 'profile_id';

	public function carts()
	{
		return $this->hasMany('carts');
	}

	public function orders()
	{
		return $this->hasMany('orders');
	}

	public function activities()
	{
		
	}

}

?>