<?php 
class Organisation extends Eloquent 
{
	protected $table = 'organisations';
    protected $primaryKey = 'organisation_id';

    public function users()
    {
    	return $this->belongsToMany('User', 'organisationshaveusers')->withPivot('owner', 'member');
    }

    public function images()
    {
    	return $this->belongsToMany('Image','organisations_have_images');
    }

    public function parent()
    {
        //return $this->hasOne('Category', 'category_id', 'parent_id');

        return $this->belongsTo('Organisation', 'parent_id', 'organisation_id');
    }

    public function children()
    {
        return $this->hasMany('Organisation','parent_id','organisation_id');
    }

    
}
?>