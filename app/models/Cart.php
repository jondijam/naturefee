<?php 
class Cart extends Eloquent
{
	protected $table = 'cart';
	protected $primaryKey = 'cart_id';

	public function activity()
	{
		return $this->belongsTo('Activity');
	}

	public function user()
	{
		
	}
}
?>