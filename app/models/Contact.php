<?php 
class Contact extends Eloquent 
{

    protected $table = 'contact';
    protected $primaryKey = 'contact_id';

    public function user()
    {
    	return $this->belongsTo('User');
    }

    public function Category()
    {
    	return $this->belongsTo('Category');
    }
}
?>