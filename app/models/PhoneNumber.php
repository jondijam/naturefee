<?php 
class PhoneNumber extends Eloquent 
{
	protected $table = 'phone_numbers';
    protected $primaryKey = 'phone_number_id';

    public function profile()
    {
    	 return $this->belongsTo('Profile');
    }

}
?>