<?php 
class HomeController extends BaseController 
{
	public function home()
	{
		//define
		(array) $data = array();
		(string) $locale = "";

		$locale = Config::get('app.locale');
		$data['locale'] = $locale;

		return View::make('index', $data);
	}

	public function dettifoss()
	{
		(string) $locale = "";
		(array) $data = array();

		$locale =  Config::get('app.locale');
		$data['locale'] = $locale;

		if ($locale == "en") 
		{
			return View::make('en/dettifoss', $data);
			# code...
		}
		else
		{
			return View::make('dettifoss', $data);
		}
	}

	public function hverir()
	{
		(string) $locale = "";
		(array) $data = array();

		$locale =  Config::get('app.locale');
		$data['locale'] = $locale;


		if ($locale == "en") 
		{
			return View::make('en/hverir', $data);
			# code...
		}
		else
		{
			return View::make('hverir', $data);
		}
	}

	public function leirhnjukur()
	{
		(string) $locale = "";
		(array) $data = array();

		$locale =  Config::get('app.locale');
		
		$data['locale'] = $locale;

		if ($locale == "en") 
		{
			return View::make('en/leirhnjukur', $data);
			# code...
		}
		else
		{
			return View::make('leirhnjukur', $data);
		}		
	}

	public function contact()
	{
		$data = array();
		$locale =  Config::get('app.locale');
		$robotest = Input::get('robotest');
		$subject = '[Naturefee.com] ';
		$from_name = Input::get('name'); 
		$from_email = Input::get('email');	
		$textMessage = Input::get('message');

		

		if($robotest)
		{
			$error = "You are a gutless robot. Get out of here, maggot!";
		}
		else
		{
			if($from_name != null && $from_email != null && $textMessage != null)
			{
				$data['textMessage'] = $textMessage;

				Mail::send(array('text' => 'emails.contact'), $data, function($message) use ($from_email, $from_name)
				{
				    $message->from($from_email, $from_name);
				    $message->to('jondi@formastudios.com')->cc('olih@oson.is')->subject('Message from Contact box');
				});

				if ($locale == "is") 
				{
					$success = utf8_decode("Takk fyrir að hafa samband, við munum svara eins fljótt og við mögulega getum.");
				}
				else
				{
					$success = "Thanks for contacting us, we'll be in touch as soon as we can.";
				}
			}
			else
			{
				
        	
        		if($locale == "is")
        		{
        			$error = "Allir reitir þurfa að vera útfylltir, vinsamlegast reyndu aftur.";
        		}
        		else
        		{
        			$error = "Every field is required, please try again.";
        		}
        		
			}

			if(isset($error))
	        {
	            echo '<div class="alert alert-danger"><i class="icon-ok-sign" style="margin-right:7px;"></i>'.$error.'</div>';
	        }     
	        else
	        {
	            echo '<div class="alert alert-success"><i class="icon-remove-sign" style="margin-right:7px;"></i>'.utf8_encode($success).'</div>';
	        }
		}
	}

	public function addToCart()
	{
		(array) $products = array();
		(array) $activities = array();
		(string) $session_id = "";
		(double) $unitAmount = 0;
		(double) $subtotal = 0;

		$products = Input::get('products');
		$session_id = Session::getId();
		

		foreach ($products as $product) 
		{
			$activity = Activity::find($product);
			$carts = Cart::where('session_id', $session_id)->where('activity_id', $product)->get();

			if (count($carts) > 0) 
			{
				foreach ($carts as $cart_entry) 
				{
					$cart = Cart::find($cart_entry->cart_id);
					$qty = $cart_entry->qty;
					$cart->qty = $qty;
					$cart->subtotal = $activity->unit_amount * $qty;
					$cart->save();
				}
			}
			else
			{
				$newcart = new Cart;
				$newcart->activity_id = $activity->activity_id;
				$newcart->session_id = $session_id;
				$newcart->qty = 1;
				
				$subtotal = ($activity->unit_amount * 1);

				$newcart->subtotal = $subtotal;
				$newcart->save();
			}
		}

		/*
		foreach ($activities as $activity) 
		{
			foreach ($products as $product) 
			{
				if($activity->activity_id == $product && $activity->activity_id )
				{
					$carts = Cart::where('session_id', $session_id)->where('activity_id', $activity->activity_id)->get();
					if (count($carts) > 0) 
					{
						foreach ($carts as $cart_entry) 
						{
							$cart = Cart::find($cart_entry->cart_id);
							$qty = $cart_entry->qty;
							$cart->qty = $qty;

							$cart->subtotal = $activity->unit_amount * $qty;
							$cart->save();
						}
					}
					else
					{
						$newcart = new Cart;
						$newcart->activity_id = $activity->activity_id;
						$newcart->session_id = $session_id;
						$newcart->qty = 1;

						$subtotal += ($activity->unit_amount * 1);
						$newcart->subtotal = $subtotal;
						$newcart->save();
					}
				}
			}
		}
		*/
	}

	public function cart()
	{
		// define variables
		(array) $data = array();
		(array) $carts = array();
		(string) $session_id = "";
		(string) $locale = "";

		//inputs 
		$session_id = Session::getId();
		$carts = Cart::where('session_id', $session_id)->get();
		$locale = Config::get('app.locale');
		//method

		//results
		$data['locale'] = $locale;
		$data['carts'] = $carts;

		return View::make('cart', $data);
	}

	public function cartUpdate($cart_id)
	{
		$qty = Input::get('qty');
		echo $qty;
		$cart = Cart::find($cart_id);
		$cart->qty = $qty;
		$cart->save();
	}


	public function cartDelete($cart_id)
	{
		$cart = Cart::find($cart_id);
		$cart->delete();

		return Redirect::to('cart');
	}

	//User need to add information about the client 
	public function information()
	{
		//define
		(string) $digitalSignature = "";
		(string) $paymentSuccesfulURL = "";
		(string) $serverUrl = "";
		(string) $cancelPayment = "";
		(array) $products = array();

	
		//input
		$locales = Lang::get( 'navbar.option' );
		$locale = Config::get('app.locale');
		$session_id = Session::getId();
		$carts = Cart::where('session_id', $session_id)->get();
		$verificationCode = 12345;
		$authorizationOnly = 0;

		//$url = route('confirm');
		//$serverUrl = route('payment');
		//$cancelPayment = route('');

		$paymentSuccesfulURL = URL::to('/confirm');
		$serverUrl = Url::to('/serverUrl');
		$cancelPayment = Url::to('/');
		//method

		//results
		$data['locale'] = $locale;
		$data['carts'] = $carts;
		$data['paymentSuccesfulURL}'] = $paymentSuccesfulURL;
		$data['cancelPayment'] = $cancelPayment;
		$data['serverUrl'] = $serverUrl;

		return View::make('information', $data);
	}

	public function register()
	{
		(array) $carts = array();
		(array) $cartsSession = array();
		(array) $json = array();

		(string) $name = "";
		(string) $email = "";
		(string) $session_id = "";
		(boolean) $passed = false;

		(int) $order_id = 0;
		(int) $profile_id = 0;
		(double) $totalPrize = 0;

		(string) $digitalSignature = "";
		(string) $paymentSuccesfulURL = "";
		(string) $serverUrl = "";
		(string) $cancelPayment = "";

		//input
		$name = Input::get('name');
		$email = Input::get('email');
		$session_id = Session::getId();


		$carts = array();
		$activities = array();
		$paraments = array();

		$validator = Validator::make(
		    array(
		        'name' => $name,
		        'email' => $email
		    ),
		    array(
		        'name' => 'required',
		        'email' => 'required|email'
		    )
	    );

	    if ($validator->passes()) 
	    {
	    	$cartsSession = Cart::where('session_id', $session_id)->get();
	    	$profile = Profile::where('email', $email)->get();

	    	if (count($profile) == 0) 
	    	{
	    		# code...
	    		$newprofile = new Profile;
		    	$newprofile->name = $name;
		    	$newprofile->email = $email;
		    	$newprofile->session_id = $session_id;
		    	$newprofile->user_id = 0;
		    	$newprofile->save();

		    	$profile_id = $newprofile->profile_id;	 
	    	}
	    	else
	    	{
	    		foreach ($profile as $pro) 
	    		{
	    			$profile_id = $pro->profile_id; 	# code...
	    		} 
	    	}

	    	foreach ($cartsSession as $cartSession) 
	    	{
	    		$totalPrize += ($cartSession->activity->unit_amount * $cartSession->qty);
	    		$paraments['subtotal'] = $cartSession->activity->unit_amount * $cartSession->qty;
	    		$paraments['qty'] = $cartSession->qty;

	    		$activities[$cartSession->activity_id] = $paraments;

	    		$carts = Cart::find($cartSession->cart_id);
	    		$carts->profile_id = $profile_id;
	    		$carts->save();
	    	}

	    	$neworder = new Order;
		    $neworder->profile_id = $profile_id;
			$neworder->total = $totalPrize;
			$neworder->barcode_number = "0";
			$neworder->save();

			$order_id = $neworder->order_id;
			
			$order = Order::find($order_id);
			$order->save();

			$order->activities()->sync($activities);

	    	
	    	$passed = true;
	    }
	    else
	    {
	    	$passed = false;
	    }

	    $json = array('passed'=>$passed, 'order_id'=>$order_id);

	    echo json_encode($json);
	}

	public function order($order_id)
	{
		(array) $data = array();
		(array) $order = array();
		(array) $activities = array();
		(array) $locales = array();
		(string) $session_id = "";
		(string) $locale = "";
		(string) $name = "";
		(string) $email = "";
		(string) $paymentSuccesfulURL = "";
		(string) $serverUrl = "";
		(string) $digitalSignature = "";
		(string) $merchant_id = "";
		(string) $verificationCode = "";


		$order = Order::find($order_id);
		$activities = $order->activities;
		$name = $order->profile->name;
		$email = $order->profile->email;
		
		$merchant_id = 858;
		$test_merchant_id = 1;
		$secretkey = "3hezxii4t7rktq5";
		$test_secretkey = "12345";

		$paymentSuccesfulURL = URL::to('/confirm/');
		$serverUrl = Url::to('/serverUrl/');
		$cancelPayment = Url::to('/');
		$order_id = $order_id;

		$digitalSignature .= $secretkey;
		$digitalSignature .= 0;

		foreach ($order->activities as $activity) 
		{
			if($activity->unit_amount > 0)
			{

				$digitalSignature .= $activity->pivot->qty;
				$digitalSignature .= $activity->unit_amount;
				$digitalSignature .= '0';
			}
			# code...
		}
		
		$digitalSignature .= $merchant_id;
		$digitalSignature .= $order_id;
		$digitalSignature .= $paymentSuccesfulURL;
		$digitalSignature .= $serverUrl;
		$digitalSignature .= 'ISK';

		$signature = md5($digitalSignature);

		$locales = Lang::get( 'navbar.option' );
		$locale = Config::get('app.locale');
		
		$data['activities'] = $order->activities;
		$data['locale'] = $locale;
		$data['order'] = $order;
		$data['name'] = $name;
		$data['email'] = $email;
		$data['paymentSuccesfulURL'] = $paymentSuccesfulURL;
		$data['serverUrl'] = $serverUrl;
		$data['cancelPayment'] = $cancelPayment;
		$data['digitalSignature'] = $signature;
		$data['order_id'] = $order_id;
		$data['merchant_id'] = $merchant_id;
		$data['count'] = 0;

		return View::make('order', $data);
	}

	public function confirm()
	{
		//echo $any;
		(array) $data = array();
		(string) $cardType = "";
		(string) $cardNumberMasked = "";
		(string) $date = "";
		(string) $transactionNumber = "";
		(array) $sales = array();
		(array) $salesBooking = array();
		(int) $referenceNumber = "";

		$locale = Config::get('app.locale');
		$data['locale'] = $locale;

		$cardType = $_GET['CardType'];
		$cardNumberMasked = $_GET['CardNumberMasked'];
		$date = $_GET['Date'];
		$transactionNumber = $_GET['TransactionNumber'];
		$referenceNumber = $_GET['ReferenceNumber'];
		$digitalSignatureResponse = $_GET['DigitalSignatureResponse'];

		if ($digitalSignatureResponse != "" && $cardType !="" && $transactionNumber != "")
		{
			$order_id = $referenceNumber;
			$order = Order::find($order_id);
			$order->payment_status = true;
			$order->payment_method = $cardType;
			$order->transaction_number = $transactionNumber;
			$order->paid_at = $date;
			$order->save();

			
			$activities = $order->activities()->get();
			$count = 0;

			foreach ($activities as $activity) 
			{
				
				if($activity->ProductId > 0)
				{
					$count++;
					$sales[$count]['activity_id'] = $activity->activity_id;
					$sales[$count]['product_id'] = $activity->ProductId;
				}
			}

			$salesBooking = $this::bookSale($sales, $order_id);

			$countBooking = 0; 

			foreach ($salesBooking as $salesBooking_entries) 
			{
				$countBooking++; 
				foreach ($activities as $activity)
				{
					if($salesBooking['order'][$countBooking]['activity_id'] == $activity->activity_id)
					{
						$activity->pivot->label = $salesBooking['order'][$countBooking]['label'];
						$activity->pivot->QRCode = $salesBooking['order'][$countBooking]['QRCode'];
						$activity->pivot->ValidFrom = $salesBooking['order'][$countBooking]['validFrom'];
						$activity->pivot->ValidTo = $salesBooking['order'][$countBooking]['validTo'];
						$activity->pivot->save();
					}
				}
			}
			
			$locale = Config::get('app.locale');

			$email = $order->Profile->email;
			$name = $order->Profile->name;
			$data['email'] = $email;
			$data['activities'] = $activities;
			$data['locale'] = $locale;
			$data['transactionNumber'] = $transactionNumber;
			$data['order_id'] = $order_id;

			Mail::send('emails.transaction', $data, function($message)
			{	
				$message->from('natturugjald@natturugjald.is', 'Náttúrugjald');
			    $message->to('olih@oson.is')->subject('Færsla '.$transactionNumber.'');
			});
		
			Mail::send('emails.number', $data, function($message) use($email, $name)
			{
				$message->from('natturugjald@natturugjald.is', 'Náttúrugjald');
				$message->to($email, $name)->subject(Lang::get('navbar.Ticket'));
			});
		

		}


	


		return View::make('confirm', $data);

		//$cardType = $_GET['CardType'];
		//$cardNumberMasked = $_GET['CardNumberMasked'];
		//$date = $_GET['Date'];
		//$transactionNumber = $_GET['TransactionNumber'];
		//$referenceNumber = $_GET['ReferenceNumber'];

		/*$cardType = $_GET['CardType'];
		$cardNumberMasked = $_GET['CardNumberMasked'];
		$date = $_GET['Date'];
		$transactionNumber = $_GET['TransactionNumber'];
		$referenceNumber = $_GET['ReferenceNumber'];
		$order_id = $referenceNumber - 1000;
		
		$digitalSignatureResponse = $_GET['DigitalSignatureResponse'];

		

		if ($digitalSignatureResponse != "")
		{
			$order = Order::find($order_id);
			$order->payment_status = true;
			$order->payment_method = $cardType;
			$order->transaction_number = $transactionNumber;
			$order->paid_at = $date;
			$order->save();

			$profile = $order->Profile;
			$activities = $order->activities()->get();

			foreach ($activities as $activity) 
			{
				$sales['activity_id'] = $activity->$activity_id;
				$sales['product_id'] = $activity->ProductId;
			}

			$salesBooking = Leidir\Bergrisi::bookSale($sales, $order_id);
			$salesBooking['order'];

			foreach ($salesBooking['order'] as $salesBooking_entries) 
			{	
				foreach ($activities as $activity) 
				{
					if($salesBooking_entries['activity_id'] == $activity->$activity_id)
					{
						$activity->pivot->label = $salesBooking_entries['label'];
						$activity->pivot->QRCode = $salesBooking_entries['QRCode'];
						$activity->pivot->ValidFrom = $salesBooking_entries['ValidFrom'];
						$activity->pivot->ValidTo = $salesBooking_entries['ValidTo'];
						$activity->pivot->save();
					}
					# code...
				}
				# code...
			}

			$locale = Config::get('app.locale');

			$data['activities'] = $activities;
			$data['locale'] = $locale;
			$data['transactionNumber'] = $transactionNumber;
			$data['order_id'] = $order_id;

			Session::flush(); 

			//senda email til eigandans

			Mail::send('emails.transaction', $data, function($message)
			{	
			    $message->to('olih@oson.is')->subject('Færsla '.$transactionNumber.'');
			});

			Mail::send('emails.number', $data, function($message)
			{
				$message->to($profile->email)->subject(Lang::get('navbar.Ticket'));
			});

		}

		
		//senda email til kúnna

		
		//sýna pöntun
		//búa til miða
		//vista barcode númer á pöntun.

		return View::make('confirm', $data);

		//Leidir\Bergrisi::bookSale($sales);
		*/
		//return View::make('confirm', $data);
	}

	public function test()
	{
		//var_dump($_GET);
		//var_dump($_SERVER);

		echo $this::notify();
	}
}
?>