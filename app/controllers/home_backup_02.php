<?php 
class HomeController extends BaseController 
{
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	//Homepage of the naturefee
	public function home()
	{
		(array) $data = array();
		(array) $locales = array();
		(string) $locale = "";

		//getting array of languages
		$locales = Lang::get( 'navbar.option');

		Leidir/Bergrisi::

		//get current language
		$locale = Config::get('app.locale');

		$data['locale'] = $locale;
		return View::make('index', $data);
	}
	//End of home page

	//hverir
	public function hverir()
	{
		(string) $locale = "";
		(array) $data = array();

		$locale =  Config::get('app.locale');
		$data['locale'] = $locale;


		if ($locale == "en") 
		{
			return View::make('en/hverir', $data);
			# code...
		}
		else
		{
			return View::make('hverir', $data);
		}
		
	}
	//end of hverir

	//dettifoss
	public function dettifoss()
	{	
		(string) $locale = "";
		(array) $data = array();

		$locale =  Config::get('app.locale');
		$data['locale'] = $locale;


		if ($locale == "en") 
		{
			return View::make('en/dettifoss', $data);
			# code...
		}
		else
		{
			return View::make('dettifoss', $data);
		}
	}	
	//endofdettifoss

	//leirhnjukur
	public function leirhnjukur()
	{
		(string) $locale = "";
		(array) $data = array();

		$locale =  Config::get('app.locale');
		
		$data['locale'] = $locale;

		if ($locale == "en") 
		{
			return View::make('en/leirhnjukur', $data);
			# code...
		}
		else
		{
			return View::make('leirhnjukur', $data);
		}		
	}
	//end of leirhnjukur

	//contact
	public function contact()
	{
		$to = 'natturugjald@natturugjald.is';
        $subject = '[Naturefee.com] ';
        $from_name = utf8_decode($_POST['name']);
        $from_email = $_POST['email'];
        $message = utf8_decode($_POST['message']);
        $robotest = $_POST['robotest'];

        //BookSale2Response BookSale2(BookSale2 $parameters)

        
	}	
	//end of contact



	//A user add products to cart
	public function addToCart()
	{
		(array) $products = array();
		(array) $activities = array();
		(string) $session_id = "";
		(double) $unitAmount = 0;
		(double) $subtotal = 0;

		
		$products = Input::get('products');
		$session_id = Session::getId();
		
		$activities = Activity::all();
		

		foreach ($activities as $activity) 
		{
			$session_id = Session::getId();
			$carts = Cart::where('session_id', $session_id)->where('activity_id', $activity->activity_id)->get();

			if (count($carts) > 0) 
			{
				foreach ($carts as $cart_entry) 
				{
					$cart = Cart::find($cart_entry->cart_id);
					$qty = $cart_entry->qty + 1;
					$cart->qty = $qty;

					$cart->subtotal = $activity->unit_amount * $qty;
					$cart->save();
					# code...
				}
				# code...
			}
			else
			{
				$newcart = new Cart;
				$newcart->activity_id = $activity->activity_id;
				$newcart->session_id = $session_id;
				$newcart->qty = 1;

				$subtotal += ($activity->unit_amount * 1);
				$newcart->subtotal = $subtotal;
				$newcart->save();
			}

			
		}
	}

	// A user go to his cart and show free tickets and tickets that cost
	public function cart()
	{
		// define variables
		(array) $data = array();
		(array) $carts = array();
		(string) $session_id = "";
		(string) $locale = "";

		//inputs 
		$session_id = Session::getId();
		$carts = Cart::where('session_id', $session_id)->get();
		$locale = Config::get('app.locale');

		//method

		//results
		$data['locale'] = $locale;
		$data['carts'] = $carts;

		return View::make('cart', $data);
	}

	public function cartUpdate($cart_id)
	{
		$qty = Input::get('qty');
		echo $qty;
		$cart = Cart::find($cart_id);
		$cart->qty = $qty;
		$cart->save();
	}

	public function cartDelete($cart_id)
	{
		$cart = Cart::find($cart_id);
		$cart->delete();

		return Redirect::to('cart');
	}

	//User need to add information about the client 
	public function information()
	{
		//define
		(string) $digitalSignature = "";
		(string) $paymentSuccesfulURL = "";
		(string) $serverUrl = "";
		(string) $cancelPayment = "";
		(array) $products = array();

	
		//input
		$locales = Lang::get( 'navbar.option' );
		$locale = Config::get('app.locale');
		$session_id = Session::getId();
		$carts = Cart::where('session_id', $session_id)->get();
		$verificationCode = 12345;
		$authorizationOnly = 0;

		//$url = route('confirm');
		//$serverUrl = route('payment');
		//$cancelPayment = route('');

		$paymentSuccesfulURL = URL::to('/confirm');
		$serverUrl = Url::to('/serverUrl');
		$cancelPayment = Url::to('/');
		//method

		//results
		$data['locale'] = $locale;
		$data['carts'] = $carts;
		$data['paymentSuccesfulURL}'] = $paymentSuccesfulURL;
		$data['cancelPayment'] = $cancelPayment;
		$data['serverUrl'] = $serverUrl;

		return View::make('information', $data);
	}

	public function order($order_id)
	{
		(array) $data = array();
		(array) $order = array();
		(array) $activities = array();
		(array) $locales = array();
		(string) $session_id = "";
		(string) $locale = "";
		(string) $name = "";
		(string) $email = "";
		(string) $paymentSuccesfulURL = "";
		(string) $serverUrl = "";
		(string) $digitalSignature = "";
		(string) $merchant_id = "";
		(string) $verificationCode = "";


		$order = Order::find($order_id);
		$activities = $order->activities;
		$name = $order->profile->name;
		$email = $order->profile->email;
		$merchant_id = 1;
		$paymentSuccesfulURL = URL::to('/confirm');
		$serverUrl = Url::to('/serverUrl');
		$cancelPayment = Url::to('/');

		$digitalSignature .= "3hezxii4t7rktq5";
		$digitalSignature .= 0;

		foreach ($order->activities as $activity) 
		{
			if($activity->unit_amount > 0)
			{

				$digitalSignature .= $activity->pivot->qty;
				$digitalSignature .= $activity->unit_amount;
				$digitalSignature .= '0';
			}
			# code...
		}
		$digitalSignature .= 858;
		$digitalSignature .= '19851'.$order_id;
		$digitalSignature .= $paymentSuccesfulURL;
		$digitalSignature .= $serverUrl;
		$digitalSignature .= 'ISK';

		$signature = md5($digitalSignature);

		$locales = Lang::get( 'navbar.option' );
		$locale = Config::get('app.locale');
		
		$data['activities'] = $order->activities;
		$data['locale'] = $locale;
		$data['order'] = $order;
		$data['name'] = $name;
		$data['email'] = $email;
		$data['paymentSuccesfulURL'] = $paymentSuccesfulURL;
		$data['serverUrl'] = $serverUrl;
		$data['cancelPayment'] = $cancelPayment;
		$data['digitalSignature'] = $signature;
		$data['order_id'] = $order_id;

		return View::make('order', $data);
	}

	//The client is registered
	public function register()
	{
		(array) $carts = array();
		(array) $cartsSession = array();
		(array) $json = array();

		(string) $name = "";
		(string) $email = "";
		(string) $session_id = "";
		(boolean) $passed = false;

		(int) $order_id = 0;
		(int) $profile_id = 0;
		(double) $totalPrize = 0;

		(string) $digitalSignature = "";
		(string) $paymentSuccesfulURL = "";
		(string) $serverUrl = "";
		(string) $cancelPayment = "";

		//input
		$name = Input::get('name');
		$email = Input::get('email');
		$session_id = Session::getId();

		$carts = array();
		$activities = array();
		$paraments = array();

		$validator = Validator::make(
		    array(
		        'name' => $name,
		        'email' => $email
		    ),
		    array(
		        'name' => 'required',
		        'email' => 'required|email'
		    )
	    );

	    if ($validator->passes()) 
	    {

	    	$cartsSession = Cart::where('session_id', $session_id)->get();
	    	$profile = Profile::where('email', $email)->get();

	    	if (count($profile) == 0) 
	    	{
	    		# code...
	    		$newprofile = new Profile;
		    	$newprofile->name = $name;
		    	$newprofile->email = $email;
		    	$newprofile->session_id = $session_id;
		    	$newprofile->user_id = 0;
		    	$newprofile->save();

		    	$profile_id = $newprofile->profile_id;	 
	    	}
	    	else
	    	{
	    		$profile_id = $profile->profile_id;	 
	    	}
	    
	    
	    	
	    	
	    	foreach ($cartsSession as $cartSession) 
	    	{
	    		$totalPrize += $cartSession->subtotal;
	    		$paraments['subtotal'] = $cartSession->subtotal;
	    		$paraments['qty'] = $cartSession->qty;

	    		$activities[$cartSession->activity_id] = $paraments;

	    		//array_push($activities, $cartSession->activity_id => array('subtotal'=> $cartSession->subtotal));
	    		

	    		$carts = Cart::find($cartSession->cart_id);
	    		$carts->profile_id = $profile_id;
	    		$carts->save();
	    	}

	    	$neworder = new Order;
		    $neworder->profile_id = $profile_id;
			$neworder->total = $totalPrize;
			$neworder->barcode_number = "0";
			$neworder->save();

			$order_id = $neworder->order_id;
			$order = Order::find($order_id);
			$order->activities()->sync($activities);
			$order->save();

	    	//print_r($activities);

	    	/*
	    	$neworder = new Order;
		    $neworder->profile_id = $profile_id;
			$neworder->total = $totalPrize;
			$neworder->barcode_number = "0";
			$neworder->save();

			$order_id = $neworder->order_id;
			$order = Order::find($order_id);
			$order->save();

			$order->activities()->sync($activities);

			attach(1, ['objectstate_id' => '27']);

			foreach ($cartsSession as $cartSession) 
			{

			}

			


			
			/*
			$order_id = $order->order_id;

			$order = Order::find($order_id);

			foreach ($cartSession as $cartSession) 
			{
				$order->pivot->subtotal = $cartSession->subtotal;
				$order->pivot->qty = $cartSession->qty;
				$order->pivot->save();
				# code...
			}
			*/

	    	$passed = true;
	    }
	    else
	    {
	    	$passed = false;
	    }

	    $json = array('passed'=>$passed, 'order_id'=>$order_id);

	    echo json_encode($json);
	}

	//the user pays on Valitor website and return back. 
	public function confirm()
	{
		$digitalSignature = "";

		$cardType = $_GET['CardType'];
		$cardNumberMasked = $_GET['CardNumberMasked'];
		$date = $_GET['Date'];
		$transactionNumber = $_GET['TransactionNumber'];
		$referenceNumber = $_GET['ReferenceNumber'];
		$order_id = $referenceNumber - 1985100;
		$order = Order::find($order_id);
		$paymentSuccesfulURL = URL::to('/confirm');
		$serverUrl = Url::to('/serverUrl');
		$digitalSignatureResponse = $_GET['DigitalSignatureResponse'];

		$digitalSignature .= 12345;
		$digitalSignature .= 0;

		foreach ($order->activities as $activity) 
		{
			if($activity->unit_amount > 0)
			{

				$digitalSignature .= $activity->pivot->qty;
				$digitalSignature .= $activity->unit_amount;
				$digitalSignature .= '0';
			}
			# code...
		}
		$digitalSignature .= 1;
		$digitalSignature .= $referenceNumber;
		$digitalSignature .= $paymentSuccesfulURL;
		$digitalSignature .= $serverUrl;
		$digitalSignature .= 'ISK';

		$signature = md5($digitalSignature);

		if (!empty($digitalSignatureResponse)) 
		{

			$gate_code = str_pad(rand(0, 9999), 4, '0', STR_PAD_LEFT);

			$order = Order::find($order_id);
			$order->payment_status = true;
			$order->payment_method = 'Cretidcard';
			$order->transaction_number = $transactionNumber;
			$order->paid_at = $date;
			$order->gate_code = $gate_code;

			$t = strtotime($date);
			$t2 = strtotime('+1 year', $t);
			$order->valid_at = date('Y-m-d', $t2);
			$order->save();


		}

		$locale = Config::get('app.locale');

		$data['activities'] = $order->activities;
		$data['locale'] = $locale;
		$data['gateCode'] = $gate_code;

		//senda email til eigandans
		/*
		Mail::send('emails.transaction', $data, function($message)
		{	
		    $message->to('olih@oson.is')->subject('Færsla '.$transactionNumber.'');
		});

		Mail::send('emails.number', $data, function($message)
		{
		    $message->to($order->profile->email)->subject('Númerið þitt er '.$gate_code.'');
		});
		*/

		//senda email til kúnna

		
		//sýna pöntun
		//búa til miða
		//vista barcode númer á pöntun.

		return View::make('confirm', $data);
	}

	//Valitor post values to the orders
	public function orderStatus()
	{
		//check if order is paid
		//if the order is paid registered the order get a id number
	}

	//the order has been used and it is signed when the client use it. 
	public function checkCode()
	{
		return View::make('checkCode');
	}	

	


	
}
?>