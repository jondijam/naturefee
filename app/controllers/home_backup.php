<?php

class HomeController extends BaseController 
{

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function home()
	{
		$data = array();
		$locales = Lang::get( 'navbar.option');
		$locale = Config::get('app.locale');
		

		$data['Locale'] = strtoupper($locale);
		$data['locale'] = $locale;

		//var_dump($locales);
		return View::make('index', $data);

	}

	public function cart()
	{
		(array) $data = array();
		(array) $carts = array();
		(array) $locales = array();

		(string) $session_id = "";
		(string) $locale = "";

		$session_id = Session::getId();
		$carts = Cart::where('session_id', $session_id)->get();

		$locales = Lang::get( 'navbar.option' );
		$locale = Config::get('app.locale');

		$data['locale'] = $locale;
		$data['carts'] = $carts;
		
		return View::make('cart', $data);
	}

	public function addToCart()
	{
		$products = Input::get('products');
		$session_id = Session::getId();
		$unit_amount = 800;
		foreach ($products as $product_id) 
		{
			$cart = new Cart;
			$cart->activity_id = $product_id;
			$cart->session_id = $session_id;
			$cart->qty = 1;
			$cart->subtotal = $unit_amount * 1;
			$cart->save();
			# code...
		}



		//user select trips
		/*
		$post = "";
		$session_id = Session::getId();
		$products = Input::get('products');
		$date = date('Y-m-d H:i:s',time());
		$accesskey = "8886273dbb2847628151a0db5645ff77";
		$http = "POST";
		$path = "https://api.bokun.is/shopping-cart.json/session/".$session_id."/activity?lang=EN&currency=ISK";
		$string = $date.$accesskey.$http."/shopping-cart.json/session/".$session_id."/activity?lang=EN&currency=ISK";
		$signature =  hash_hmac('sha1', $string, "a0037efbeef3495fa73d99f72226d802", true);
		$hash = base64_encode($signature);
		
		foreach ($products as $product_id) 
		{
			$post = '{"": 2,"startDate": "2013-11-11","endDate": "2014-06-12"}';




			# code...
		}
		// trips is added to the shopping cart

		// a user is created

		// user login and checkout the booking
		*/
	}

	public function updateCart($cart_id)
	{

	}

	public function deleteCart($cart_id)
	{

	}

	public function information()
	{
		$locales = Lang::get( 'navbar.option' );
		$locale = Config::get('app.locale');
		$total = 0;
		$session_id = Session::getId();
		$carts = Cart::where('session_id', $session_id)->get();

	
		$url = route('confirm');
		$serverUrl = route('payment');
		$cancelPayment = route('');



		$data['Locale'] = strtoupper($locale);
		$data['locale'] = $locale;
		$data['carts'] = $carts;
		$data['url'] = $url;
		$data['cancelPayment'] = $cancelPayment;
		$data['serverUrl'] = $serverUrl;

		return View::make('information', $data);
	}

	public function register()
	{
		$name = "";
		$email = "";

		$name = Input::get('name');
		$email = Input::get('email');
		$session_id = Session::getId();

		$validator = Validator::make(
	    array(
	        'name' => $name,
	        'email' => $email
	    ),
	    array(
	        'name' => 'required',
	        'email' => 'required|email|unique:profiles'
	    )
	    );

	    if ($validator->passes()) 
	    {
	    	$total = 0;
	    	echo 'passed';

	    	$profile = new Profile;
	    	$profile->name;
	    	$profile->email;
	    	$profile->save();
	    	$profile_id = $profile->profile_id;

	    	$carts = $carts->where('session_id', $session_id);

	    	foreach ($carts as $cart_entry) 
	    	{
	    		$cart = Cart::find($cart_entry->cart_id);
	    		$cart->profile_id = $profile_id;
	    		$total += $cart->prize;
	    		$cart->save();
	    		# code...
	    	}

			$order = new Order;
			$order->profile_id = $profile_id;

			$order->total = $total;
			$order->barcode_number = "0";
			$order->save();

				    	
	    	# code...
	    }
	    else
	    {
	    	$messages = $validator->messages();
	    	echo 'failed';
	    	foreach ($messages->all() as $message)
			{
				    echo $message;
			}
	    }

		//validation
	}

	public function payment()
	{
		return View::make('payment');
	}

	public function checkout()
	{
		return View::make('checkout');
	}


}
