<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
     * Action used to set the application locale.
     * 
     */

	public function setLocale()
	{
		$mLocale = Request::segment( 2, Config::get( 'app.locale' ) ); // Get parameter from URL.
        if ( in_array( $mLocale , Config::get( 'app.locales' ) ) )
        {
           App::setLocale( $mLocale );
           Session::put( 'locale', $mLocale );
           Cookie::forever( 'locale', $mLocale );
        }
        return Redirect::back();
	}

	/**
     * Detect and set application localization environment (language).
     * NOTE: Don't foreget to ADD/SET/UPDATE the locales array in app/config/app.php!
     *
     */

	private function configureLocale()
	{
		 // Set default locale.
        $mLocale = Config::get( 'app.locale' );

        // Has a session locale already been set?
        if ( !Session::has( 'locale' ))
        {
        	 // No, a session locale hasn't been set.
            // Was there a cookie set from a previous visit?
            $mFromCookie = Cookie::get( 'locale', null );
            if ( $mFromCookie != null && in_array( $mFromCookie, Config::get( 'app.locales' ) ) )
            {
                // Cookie was previously set and it's a supported locale.
                $mLocale = $mFromCookie;
            }
            else
            {
                // No cookie was set.
                // Attempt to get local from current URI.
                $mFromURI = Request::segment( 1 );
                if ( $mFromURI != null && in_array( $mFromURI, Config::get( 'app.locales' ) ) )
                {
                    // supported locale
                    $mLocale = $mFromURI;
                }
                else
                {
                    // attempt to detect locale from browser.
                    $mFromBrowser = substr( Request::server( 'http_accept_language' ), 0, 2 );
                    if ( $mFromBrowser != null && in_array( $mFromBrowser, Config::get( 'app.locales' ) ) )
                    {
                        // browser lang is supported, use it.
                        $mLocale = $mFromBrowser;
                    } // $mFromBrowser
                } // $mFromURI
            } // $mFromCookie

            Session::put( 'locale', $mLocale );
            Cookie::forever( 'locale', $mLocale );
        }
        else
        {
            // session locale is available, use it.
            $mLocale = Session::get( 'locale' );
        } // Session?

        // set application locale for current session.
        App::setLocale( $mLocale );
	}


    public static function notify()
    {
        return 'User Notified';
    }

    public static function bookSale($sales, $order_id)
    {
        (string) $sellerId = "";
        (string) $sellerPassword = "";
        (string) $privateKey = "";
        (string) $referenceId = "";
        (string) $currentTime = "";
        (string) $validFrom = "";
        (string) $validTo = "";
        //(array) $salesLines[] = array();
        (array) $params = array();
        (array) $salesbooking = array();
        (string) $signature = "";
        //(object) $client = new \SoapClient("http://webservice.leidir.is/leidirtest/leidirwebservice.asmx?WSDL", array('encoding'=>'UTF-8'));
        (object) $client = new \SoapClient("http://webservice.leidir.is/leidirlive/leidirwebservice.asmx?WSDL", array('encoding'=>'UTF-8'));
        (object) $booksale = new  \stdClass();
        (object) $bookSaleResult = new  \stdClass();
        (object) $bookinglines = new  \stdClass();
        (object) $bookingline = new  \stdClass();

        $sellerId = "3648720BFEA411E387FA022016B4CAC6";
        $sellerPassword = "PalaceCourt";
        $privateKey = "364872BAFEA411E387FA022016B4CAC6";

        $test_sellerId = "2CF2981EFE9B11E387FA022016B4CAC6";
        $test_sellerPassword = "TestLykill";
        $test_privateKey = "2CF298A1FE9B11E387FA022016B4CAC6";

        $referenceId = $order_id;
        $currentTime = "".date('Y-m-d\TH:i:s')."";
        $validFrom =  date('Y-m-d\TH:i:s');
        $validTo = date("Y-m-d\TH:i:s", strtotime('+1 year')); 
        $count = 0;
    

        foreach ($sales as $key => $sale) 
        {
            $count++;
             $salesLines[] = array(
                'LineRefId'=>$sales[$count]['activity_id'], 
                'ProductId'=> $sales[$count]['product_id'],
                'ValidFrom' => $validFrom,
                'ValidTo' => $validTo,
                'AdditionalInfo' => '', 
                );
            
        }

        
        
        $signature = $sellerId.$sellerPassword.$referenceId.$currentTime.$privateKey;
        $signature = md5($signature);
        $params = array('sellerId'=>$sellerId, "sellerPassword"=>$sellerPassword,'referenceId'=>$referenceId, 'clientTimestamp'=>$currentTime, "salesLines" => $salesLines,'additionalInfo'=>'', 'md5'=>$signature);

        $booksale = $client->BookSale($params);

        $bookSaleResult = $booksale->BookSaleResult;
        $bookinglines = $bookSaleResult->BookingLine;
        $bookingline = $bookinglines->BookingLine;

        $salesbooking['order_id'] = $bookSaleResult->ReferenceId;
        //var_dump($bookingline);
        $countBooking = 0; 
        foreach ($bookingline as $booking) 
        {
            $countBooking++; 
            $salesbooking['order'][$countBooking]['activity_id'] = $booking->LineRefId;
            $salesbooking['order'][$countBooking]['label'] = $booking->Label;
            $salesbooking['order'][$countBooking]['QRCode'] = $booking->QRCode;
            $salesbooking['order'][$countBooking]['validTo'] = $validTo;
            $salesbooking['order'][$countBooking]['validFrom'] = $validFrom;
        }
        
        return $salesbooking;
        
    }

}
