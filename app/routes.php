<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/




if($_SERVER['HTTP_HOST'] == "natturugjald.local" || $_SERVER['HTTP_HOST'] == "www.natturugjald.is" || $_SERVER['HTTP_HOST'] == "natturugjald.is")
{
	\App::setLocale("is");
}
else
{
	\App::setLocale("en");
}

$languages = Config::get('app.locales');
if(!$languages)
{
    $languages = array();
}

$locale = Request::segment(1);

if(in_array($locale, $languages))
{
    \App::setLocale($locale);
}
else 
{
    $locale = null;
}

$appRoutes = function() 
{
	
		Route::get('/', 'HomeController@home');
	    Route::get('/hverir', 'HomeController@hverir');
		Route::get('/dettifoss', 'HomeController@dettifoss');
		Route::get('/leirhnjukur', 'HomeController@leirhnjukur');
		Route::post('/addToCart', 'HomeController@addToCart');

		Route::get('/cart', 'HomeController@cart');
		Route::post('/cart/update/{id}', 'HomeController@cartUpdate');
		Route::get('/cart/delete/{id}', 'HomeController@cartDelete');
		
		Route::get('/information', 'HomeController@information');
		Route::post('/information/register', 'HomeController@register');

		Route::get('/confirm','HomeController@confirm');
		Route::get('/payment','HomeController@payment');
		Route::get('/order/{id}','HomeController@order');
		Route::post('/contact', 'HomeController@contact');

		Route::get('/checkcode', 'HomeController@checkCode');	
		Route::get('/t', 'HomeController@test');	

   


	//paid
	Route::get('/checkout', 'HomeController@checkout');
};

Route::group(array('domain' => 'natturugjald.local'), $appRoutes);
Route::group(array('domain' => 'naturefee.local'), $appRoutes);

Route::group(array('domain' => 'www.natturugjald.is'), $appRoutes);
Route::group(array('domain' => 'natturugjald.is'), $appRoutes);
Route::group(array('domain' => 'naturefee.com'), $appRoutes);
Route::group(array('domain' => 'natturugjald.formastudios.com'), $appRoutes);



