<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationsHaveUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('OrganisationsHaveUsers', function($table)
		{
			$table->increments('organisation_users_id');
			$table->integer('organisation_id');
			$table->integer('user_id');
			$table->integer('role_id');
			$table->boolean('member');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('OrganisationsHaveUsers');
	}

}
