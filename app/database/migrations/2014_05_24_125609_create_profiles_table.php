<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Profile', function($table)
	    {
	        $table->increments('profile_id');
	        $table->string('name',255);
	        $table->string('birthday', 255);
	        $table->integer('marital_status');
	        $table->string('position');
	        $table->string('address',255);
	        $table->string('city',255);
	        $table->string('zip',255);
	        $table->string('country',255);
	        $table->integer('gender');
	        $table->integer('user_id');
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Profile');
	}

}
