<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function($table)
		{
		    $table->increments('profile_id');
		    $table->string('name', 255);
		    $table->string('email', 255)->unique();
		    $table->string('session_id', 255);
		    $table->integer('user_id')->default(0); 
		    $table->softDeletes();
		    $table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('profiles');
	}

}
