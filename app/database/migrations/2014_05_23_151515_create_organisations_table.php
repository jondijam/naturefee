<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Organisations', function($table)
	    {
	        $table->increments('organisation_id');
	        $table->string('name', 255);
	       	$table->string('telephone', 255)->nullable();
	       	$table->string('address', 255);
	       	$table->string('city', 255);
	       	$table->string('zip', 255);
	       	$table->string('country', 255);
	       	$table->integer('parent_id');
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Organisations');
	}

}
