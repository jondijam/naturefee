<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration 
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('users', function($table)
	    {
	        $table->increments('user_id');
	        $table->string('name', 255);
	        $table->string('email',255)->unique();
	        $table->string('username',255)->unique();
	        $table->string('password',255);
	        $table->string('remember_token', 100)->nullable();
	        $table->integer('role_id');
	        $table->integer('parentId');
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
