/* --------------------------------------------------------------------------------

  DENTA SCROLL TO TOP JQUERY PLUGIN - WORK BY B. GUNNARSSON @ DENTA www.denta.is

-------------------------------------------------------------------------------- */


(function($){	
	$.fn.scrollToTop = function(){

		var defaults = {
			scrollDuration: 400,
			scrollTop: 50
		}; // default options values

		var options = {}; // empty options object
		$.extend(options, defaults); // extend the defaults and users can pass in options


		// console.log("scrollToTop plugin has called by the first customer!");
		return this.each(function() {
			$(this).click(function(event) {
				$("html, body").animate({scrollTop: 0}, defaults.scrollDuration); // animate the body tag.
				event.preventDefault();	 // prevent default link behaviour.	
			});
			//console.log($(this).data()); // check if the data attributes are showing up.
			$(this).css($(this).data()); // plug in the data attributes from element.
			$(this).hide(); // hide link to start with
			var link = $(this); 
			// window scroll function
			$(window).scroll(function() {
				//console.log($(window).scrollTop());
				if( $(window).scrollTop() > defaults.scrollTop ) { // if user scroll distance is > default scroll distance fade the links in
					link.fadeIn();
				} else {
					link.hide();
				};
			});
		}); // end each

	} // end scrollToTop
})(jQuery)