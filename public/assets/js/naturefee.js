/* JS/JQUERY WORK BY B. GUNNARSSON www.bgunnarsson.com */

// RANDOM IMAGES FUNCTION
function randomImages() {
	var images = ['cover1.jpg', 'cover2.jpg', 'cover3.jpg', 'cover3.jpg'];
	var bgImg = 'assets/img/random/';
	$('.lang-default .cover').css({
		'background-image': 'url('+ bgImg + images[Math.floor(Math.random() * images.length)] + ')'
	});
};

$(document).ready(function()
{

	// GLOBAL VARIABLES
	var discountAmount = 0;
	var locationCost = 0;
	var vat = 1.255;
	var product_id = 0;
	var products = [];
	var id = 0;
	var location = "";
	var locale = "";
	var name = "";
	var email = "";
	var qty = 0;
	var unit_amount = 0;
	var subtotal = 0;
	var info = new Array();


	// RANDOM IMAGES
	randomImages();
	
	$('#register').click(function(e)
	{
		e.preventDefault();
		name = $('#name').val();
		email = $('#email').val();

		$.post('/information/register', {name:name, email:email}, function(json){
			
			info = JSON.parse(json);
			
			
			if (info.passed === true) 
			{	
		
				window.location = '/order/'+info.order_id+'';			
				//window.location = "/";
				//$('#registerSuccess').removeClass('hidden');
			}
			else
			{
				//errors
			}
			

			
		});
	});

	$('.checkboxin').click(function(e) 
	{
		// e.preventDefault();
		// e.stopPropagation();
		product_id = $(this).attr('data-checkbox');


		var checkbox = $(this).find('.checkbox');
		var checkText = $(this).find('.listText');
		var vatText = "";
		var currency = "";
		locale = $(this).attr('data-locale');

		if (locale == "en") 
		{
			vatText = 'Incl.VAT';
			currency = 'ISK';
		}
		else
		{
			vatText = 'Innif.VSK';
			currency = 'kr.'
		}
		
		if($(checkbox).hasClass('emptyCheck'))
		{
			$(checkbox).removeClass('emptyCheck');
			$(checkbox).addClass('fullCheck');
			$(checkText).addClass('activeText');

			if(locationCost >= 0 && product_id < 3) 
			{

				locationCost += 800;
				//$('.free').addClass('hidden');
				discountAmount = Math.round((locationCost / 1.255) * 0.255);
				$('#offHolder').fadeIn(300).html(vatText+' '+discountAmount+' '+currency);
			}
			else if(product_id == 3)
			{
				$('.activity-free').removeClass('hidden');
				//$('.free').removeClass('hidden');
			}

			
			// RAISE DISCOUNT AMOUNT
			/*if( discountAmount != 3 ) {
				discountAmount += 1;
			}
			*/
			

			/*if( discountAmount == 1 ) {
				$('#offHolder').fadeIn(300).html('162 ISK vsk');
			} else if( discountAmount == 2 ) {
				$('#offHolder').fadeIn(300).html('325 ISK vsk');
			} else if( discountAmount == 3 ) {
				$('#offHolder').fadeIn(300).html('488 ISK vsk');
			}
			*/

			// SHOW LOCATION COST
			$('#priceHolder').html(locationCost+' ISK');
		}
		else
		{
			// ADD / REMOVE APPROPRIATE CLASSES
				$(checkbox).removeClass('fullCheck');
				$(checkbox).addClass('emptyCheck');
				$(checkText).removeClass('activeText');

				if(locationCost != 0 && product_id < 3) 
				{
					locationCost -= 800;

					//innif.VSK

					discountAmount = Math.round((locationCost / 1.255) * 0.255);
					
					$('#offHolder').fadeIn(300).html(vatText+' '+discountAmount+' '+currency);
				
				}
				else if(product_id == 3)
				{
					$('.activity-free').addClass('hidden');
				}

				if( discountAmount == 0 ) 
				{
					$('#offHolder').fadeOut(300);
				}

				$('#priceHolder').html(locationCost+' ISK');
				

				/*	// LOWER DISCOUNT AMOUNT
				if( discountAmount <= 3 ) {
					discountAmount -= 1;
				}
				// HIDE DISCOUNT
				if( discountAmount < 3 ) 
				{
					// $('#offHolder').fadeOut(300);
					$('#priceHolder').html(locationCost+' ISK');
				}
			
				if( discountAmount == 0 ) 
				{
					$('#offHolder').fadeOut(300);
				}
				
				

				/*if( discountAmount == 1 ) {
					$('#offHolder').html('162 ISK vsk');
				}
				if( discountAmount == 2 ) {
					$('#offHolder').fadeIn(300).html('325 ISK vsk');
				}
				if( discountAmount == 3 ) {
					$('#offHolder').fadeIn(300).html('488 ISK vsk');
				}*/

		}

	});
	
	$('#buyTrigger').click(function(e){
		e.preventDefault();
		locale = $(this).attr('data-locale');

		$('.fullCheck').each(function()
		{
			id = $(this).parent().attr('data-checkbox');
			
			products.push(id);
			
			if (id != 3) 
			{
				products.push(3);
			};

		});

		$.post('/addToCart', {products:products}, function(data)
		{
			if (locale == "en") 
			{
				window.location = "/cart";
			}
			else
			{
				window.location = "/cart";
			}
			
		});

	});
	


	$('.update').click(function(e){
		

		e.preventDefault();
		
		product_id = $(this).attr('data-cart-id');
		unit_amount = $(this).attr('data-unit-amount');
		qty = $('#qty'+product_id).val();

		subtotal = qty * unit_amount;

		$('#'+product_id+'subtotal').text(subtotal);

		$.post('/cart/update/'+product_id, {qty:qty, product_id:product_id}, function(data){

		});

	});

	// DENTA SCROLL TO TOP
	$('.toTheTop').scrollToTop();

	// LOGO TRIGGER
	$('.logoToTop').click(function(e) {
		e.preventDefault();
		$("html, body").animate({scrollTop: 0}, 400); // animate the body tag.
	});

	// SCROLL TO SECTION
	$('.navItem').click(function(e) {
		e.preventDefault();
		var sectionNumber = $(this).attr('data-section');
		$('#section_'+sectionNumber).ScrollTo({offsetTop:65}); //offset was 65
		$('#arcade-mobile-nav').slideUp(125);
	});

	// LANGUAGE DROPDOWN
	$('#navLangTrigger').click(function(e) {
		e.preventDefault();
		if ( $('#langList').css('display') == 'none' ) {
			$('#langList').slideDown(250);
		} else {
			$('#langList').slideUp(125);
		}
	});

	/* MOBILE NAV
	--------------------------------------------------------------- */ 
	// 01. PULL MAIN NAV ITEMS INTO MOBILE NAVIGATION
	// var oldNav = $('#arcade-desktop-nav').html();
	// $(oldNav).appendTo('#arcade-nav-expansion');

	//02. CLICK MOBILE NAV TRIGGER
	$('#mobile-nav-trigger').click(function(e) {
		e.preventDefault();
		// 03. SHOW THE MOBILE NAVIGATION
		if ( $('#arcade-mobile-nav').css('display') == 'none' ) {
			$('#arcade-mobile-nav').slideDown(300);
		} else { // 04. HIDE THE NAVIGATION
			$('#arcade-mobile-nav').slideUp(100);
		}
	});

	// CONTACT FORM
	$('#sender').click(function(e){
		e.preventDefault();

		var name = $('#name').val();
		var message = $('#message').val();
		var email = $('#email').val();
		var robotest = $('#robotest').val();

		$.post('/contact', { name: name, message: message, email: email, robotest: robotest }, function(data) {
			$('#name').val('');
			$('#message').val('');
			$('#email').val('');
			$('#robotest').val('');
			$('#errormessages').show().html(data);
			// console.log(data);
		});
	}); // CONTACT FORM END

}); // DOC READY