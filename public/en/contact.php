<?php
    //phpinfo();
    $to = null;
    $subject = null;
    $from_name = null;
    $from_email = null;
    $message = null;
    $success = null;
    $error = null;

    if($_POST)
    {
        $to = 'natturugjald@natturugjald.is';
        $subject = '[Naturefee.com] ';
        $from_name = utf8_decode($_POST['name']);
        $from_email = $_POST['email'];
        $message = utf8_decode($_POST['message']);
        $robotest = $_POST['robotest'];

        if($robotest)
        {
            $error = "You are a gutless robot. Get out of here, maggot!";
        }
        else
        {
            if($from_name != null && $from_email != null && $message != null)
            {
                $header = "From: $from_name <$from_email>";
                if(mail($to, $subject, $message, $header))
                    $success = "Thanks for contacting us, we'll be in touch as soon as we can.";
                else
                    $error = "Something went amiss, please try again.";
            }
            else
            {
                 $error = "Every field is required, please try again.";

            }
        }
        if(isset($error))
        {
            echo '<div class="alert alert-danger"><i class="icon-ok-sign" style="margin-right:7px;"></i>'.$error.'</div>';
        }     
        else
        {
            echo '<div class="alert alert-success"><i class="icon-remove-sign" style="margin-right:7px;"></i>'.utf8_encode($success).'</div>';
        }
           
    }
?>